class AddressModel {
  int pk;
  int pkTask;
  String address;
  int porchCount;

  AddressModel({
    this.pk,
    this.pkTask,
    this.address,
    this.porchCount,
  });

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
      pk: json['pk'],
      pkTask: json['pk_task'],
      address: json['address'],
      porchCount: json['porch_count'],
    );
  }

  Map<String, dynamic> toMap() {
    final map = new Map<String, dynamic>();
    map["pk"] = pk;
    map["pk_task"] = pkTask;
    map["address"] = address;
    map["porch_count"] = porchCount;
    return map;
  }

  factory AddressModel.fromMap(Map<String, dynamic> data) => new AddressModel(
      pk: data['pk'],
      pkTask: data['pk_task'],
      address: data['address'],
      porchCount: data['porch_count']);
}
