class TaskModel {
  int pk;
  String city;
  int addressCount;
  int porchCount;
  String type;
  String comment;
  String date;
  int countImage;
  int imageSend;
  bool isClosedOrNotFound;

  TaskModel(
      {this.pk,
      this.addressCount,
      this.city,
      this.porchCount,
      this.type,
      this.comment,
      this.date,
      this.countImage,
      this.imageSend,
      this.isClosedOrNotFound});

  factory TaskModel.fromJson(Map<String, dynamic> json) {
    return TaskModel(
        pk: json['pk'],
        city: json['city'],
        addressCount: json['address_count'],
        porchCount: json['porch_count'],
        type: json['type'],
        comment: json['comment'],
        date: json['date'],
        isClosedOrNotFound: json['isClosedOrNotFound'] == null
            ? false
            : json['isClosedOrNotFound']);
  }

  Map<String, dynamic> toMap() {
    final map = new Map<String, dynamic>();
    map["pk"] = pk;
    map["city"] = city;
    map["address_count"] = addressCount;
    map["porch_count"] = porchCount;
    map["type"] = type;
    map["comment"] = comment;
    map["date"] = date;
    map["is_closed_or_not_found"] = isClosedOrNotFound ? 1 : 0;
    return map;
  }

  factory TaskModel.fromMap(Map<String, dynamic> data) => new TaskModel(
      pk: data['pk'],
      city: data['city'],
      addressCount: data['address_count'],
      porchCount: data['porch_count'],
      type: data['type'],
      comment: data['comment'],
      date: data['date'],
      countImage: data['countImage'],
      imageSend: data['imageSend'],
      isClosedOrNotFound: data['is_closed_or_not_found'] == 1 ? true : false);
}
