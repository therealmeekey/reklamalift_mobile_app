class PorchsModel {
  int pk;
  bool isChange;
  int pkAddress;
  String type;
  String address;
  String porchNumber;
  int porchId;
  String pkTask;

  PorchsModel(
      {this.pk,
      this.isChange,
      this.pkAddress,
      this.type,
      this.address,
      this.porchNumber,
      this.porchId,
      this.pkTask});

  factory PorchsModel.fromJson(Map<String, dynamic> json) {
    return PorchsModel(
      pk: json['pk'],
      pkTask: json['pkTask'],
      type: json['type'],
      // isChange: json['is_change'] == null ? false : json['is_change'] == 1 ? true : false,
      isChange: json['is_change'],
      pkAddress: json['pk_address'],
      address: json['address'],
      porchNumber: json['porch_number'],
      porchId: json['porch_id'],
    );
  }

  Map<String, dynamic> toMap() {
    final map = new Map<String, dynamic>();
    map["pk"] = pk;
    map["pkTask"] = pkTask;
    map["is_change"] = isChange ? 1 : 0;
    map["pk_address"] = pkAddress;
    map["type"] = type;
    map["address"] = address;
    map["porch_number"] = porchNumber;
    map["porch_id"] = porchId;
    return map;
  }

  factory PorchsModel.fromMap(Map<String, dynamic> data) => new PorchsModel(
      pk: data['pk'],
      pkTask: data['pkTask'],
      pkAddress: data['pk_address'],
      type: data['type'],
      address: data['address'],
      porchNumber: data['porch_number'],
      porchId: data['porch_id']);
}
