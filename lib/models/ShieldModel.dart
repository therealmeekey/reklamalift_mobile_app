class ShieldModel {
  int pk_address;
  int porch_id;
  String pkTask;
  bool is_send;
  bool broken_shield;
  bool broken_gib;
  bool no_glass;
  bool replace_glass;
  bool against_tenants;
  bool no_social_info;
  bool no_top_plank;
  bool no_left_plank;
  bool no_right_plank;
  bool lift_replacing;
  bool is_repaired;
  bool is_display;
  String address;
  String porchNumber;
  String img;
  String date;
  bool is_broken;

  ShieldModel(
      {this.pk_address,
      this.porch_id,
      this.pkTask,
      this.is_send,
      this.broken_shield,
      this.broken_gib,
      this.no_glass,
      this.replace_glass,
      this.against_tenants,
      this.no_social_info,
      this.no_top_plank,
      this.no_left_plank,
      this.no_right_plank,
      this.lift_replacing,
      this.is_repaired,
      this.is_display,
      this.address,
      this.porchNumber,
      this.img,
      this.date,
      this.is_broken});

  factory ShieldModel.fromJson(Map<String, dynamic> json) {
    return ShieldModel(
      is_send: json['is_send'] ?? false,
      pkTask: json['pkTask'],
      porch_id: json['porch_id'],
      pk_address: json['pk_address'],
      broken_shield: json['broken_shield'],
      broken_gib: json['broken_gib'],
      no_glass: json['no_glass'],
      replace_glass: json['replace_glass'],
      against_tenants: json['against_tenants'],
      no_social_info: json['no_social_info'],
      no_top_plank: json['no_top_plank'],
      no_left_plank: json['no_left_plank'],
      no_right_plank: json['no_right_plank'],
      lift_replacing: json['lift_replacing'],
      is_repaired: json['is_repaired'],
      is_display: json['is_display'],
      address: json['address'] ?? '',
      porchNumber: json['porchNumber'] ?? '',
      img: json['img'] ?? '',
      date: json['date'] ?? '',
      is_broken: json['is_broken'] ?? false,
    );
  }

  Map<String, dynamic> toMap() {
    final map = new Map<String, dynamic>();
    map["is_send"] = is_send;
    map["pkTask"] = pkTask;
    map["pk_address"] = pk_address;
    map["porch_id"] = porch_id;
    map["broken_shield"] = broken_shield ? 1 : 0;
    map["broken_gib"] = broken_gib ? 1 : 0;
    map["no_glass"] = no_glass ? 1 : 0;
    map["replace_glass"] = replace_glass ? 1 : 0;
    map["against_tenants"] = against_tenants ? 1 : 0;
    map["no_social_info"] = no_social_info ? 1 : 0;
    map["no_top_plank"] = no_top_plank ? 1 : 0;
    map["no_left_plank"] = no_left_plank ? 1 : 0;
    map["no_right_plank"] = no_right_plank ? 1 : 0;
    map["lift_replacing"] = lift_replacing ? 1 : 0;
    map["is_repaired"] = is_repaired ? 1 : 0;
    map["is_display"] = is_display ? 1 : 0;
    map["address"] = address;
    map["porchNumber"] = porchNumber;
    map["img"] = img;
    map["date"] = date;
    map["is_broken"] = is_broken ? 1 : 0;
    return map;
  }

  factory ShieldModel.fromMap(Map<String, dynamic> data) => new ShieldModel(
      porch_id: data['porch_id'],
      pkTask: data['pkTask'],
      pk_address: data['pk_address'],
      is_send: data['is_send'] == 1 ? true : false,
      broken_shield: data['broken_shield'] == 1 ? true : false,
      broken_gib: data['broken_gib'] == 1 ? true : false,
      no_glass: data['no_glass'] == 1 ? true : false,
      replace_glass: data['replace_glass'] == 1 ? true : false,
      against_tenants: data['against_tenants'] == 1 ? true : false,
      no_social_info: data['no_social_info'] == 1 ? true : false,
      no_top_plank: data['no_top_plank'] == 1 ? true : false,
      no_left_plank: data['no_left_plank'] == 1 ? true : false,
      no_right_plank: data['no_right_plank'] == 1 ? true : false,
      lift_replacing: data['lift_replacing'] == 1 ? true : false,
      is_repaired: data['is_repaired'] == 1 ? true : false,
      is_display: data['is_display'] == 1 ? true : false,
      address: data['address'],
      porchNumber: data['porchNumber'],
      img: data['img'],
      date: data['date'],
      is_broken: data['is_broken'] == 1 ? true : false);
}
