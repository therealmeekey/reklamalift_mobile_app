class PhotoModel {
  int id;
  String image;
  String title;
  String subtitle;
  int porchId;
  bool isBroken;

  PhotoModel(
      {this.image,
      this.title,
      this.subtitle,
      this.porchId,
      this.id,
      this.isBroken});

  factory PhotoModel.fromJson(Map<String, dynamic> json) {
    return PhotoModel(
        id: json['id'],
        image: json['image'],
        title: json['title'],
        subtitle: json['subtitle'],
        porchId: json['porchId'],
        isBroken: json['isBroken']);
  }

  factory PhotoModel.fromMap(Map<String, dynamic> data) => new PhotoModel(
      id: data['id'],
      image: data['image'] == null ? '' : '',
      title: data['title'],
      subtitle: data['subtitle'],
      porchId: data['porchId'],
      isBroken: data['isBroken']);
}
