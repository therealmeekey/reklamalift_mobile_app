class MarkersModel {
  String pk;
  String address;
  String porchCount;
  double coord_x;
  double coord_y;

  MarkersModel(
      {this.pk, this.address, this.porchCount, this.coord_x, this.coord_y});

  factory MarkersModel.fromJson(Map<String, dynamic> json) {
    return MarkersModel(
      pk: json['id'],
      address: json['address'],
      porchCount: json['porch_count'],
      coord_x: json['coord_x'],
      coord_y: json['coord_y'],
    );
  }
}
