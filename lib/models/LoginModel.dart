class LoginModel {
  final String token;
  final String email;

  LoginModel({this.token, this.email});

  factory LoginModel.fromJson(Map<String, dynamic> json) {
    return LoginModel(token: json['token'], email: json['email']);
  }
}
