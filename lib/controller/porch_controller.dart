import 'package:get/get.dart';
import 'package:reklamalift/controller/address_controller.dart';
import 'package:reklamalift/models/PorchsModel.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import '../main.dart';

class PorchController extends GetxController {
  static PorchController get to => Get.find();
  var load = false.obs;
  var data = [].obs;
  var addressIndex = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  getPorchsList() async {
    /// Выгружаем данные из БД
    try {
      load(true);
      List<Map> porchList = await databaseHelper.db.rawQuery(
          "SELECT * FROM porchs WHERE pk_address = ? AND pkTask = ? AND is_change = 0",
          [
            AddressController.to.data[int.parse(addressIndex.value)].pk,
            AddressController.to.data[int.parse(addressIndex.value)].pkTask
                .toString()
          ]);
      data.clear();
      data.assignAll(porchList.map((job) => PorchsModel.fromMap(job)).toList());
      data.refresh();
    } catch (exception, stackTrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
