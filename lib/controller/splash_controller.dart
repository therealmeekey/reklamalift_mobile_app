import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'dart:io' show Platform;
import 'package:get_storage/get_storage.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:reklamalift/http/url.dart';
import 'package:reklamalift/http/request.dart';
import 'package:reklamalift/models/AddressModel.dart';
import 'package:reklamalift/models/PorchsModel.dart';
import 'package:reklamalift/models/ShieldModel.dart';
import 'package:reklamalift/models/TaskModel.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import '../main.dart';

class SplashController extends GetxController {
  static SplashController get to => Get.find();
  var textLoader = ''.obs;

  @override
  void onInit() {
    getAllData();
    super.onInit();
  }

  checkPermission() async {
    var cameraStatus = await Permission.camera.status;
    var storageStatus = await Permission.storage.status;
    if (cameraStatus.isUndetermined || cameraStatus.isDenied) {
      await Permission.camera.request();
      if (await Permission.camera.request().isDenied) {
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      }
    }
    if (Platform.isAndroid) {
      if (storageStatus.isUndetermined || storageStatus.isDenied) {
        await Permission.storage.request();
        if (await Permission.storage.request().isDenied) {
          SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        }
      }
    }
  }

  getAllData() async {
    ///  Если есть интернет соединение:
    ///  1. Получаем список данных из БД
    ///  2. Удаляем все данные из БД
    ///  3. Загружаем обновленные данные
    try {
      await checkPermission();
      textLoader.value = 'Получаю токен пользователя.\nПодключаюсь к базе данных.\nПроверяю наличие интернета.';
      var token = await GetStorage().read('token');
      final connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        textLoader.value = 'Связь с интернетом не обнаружена.';
        Get.toNamed('/task');
        Get.lazyPut(() => TaskController());
        TaskController.to.localGetTasks();
      } else {
        textLoader.value = 'Получаю данные с reklamalift.com';
        Request request = Request(url: getData, headers: {"Authorization": "Bearer $token"});
        var batch = databaseHelper.db.batch();
        await request.get().then((value) async {
          if (value.statusCode == 200) {
            textLoader.value = 'Обрабатываю набор задач.\n' + 'Проверяю наличие дуближей,\n' + 'завершенных или удаленных задач';
            List tasks = json.decode(utf8.decode(value.bodyBytes))[0]['tasks_list'];
            List address = json.decode(utf8.decode(value.bodyBytes))[0]['address_list'];
            List porchs = json.decode(utf8.decode(value.bodyBytes))[0]['porch_list'];

            var tupleTask = tasks.map((job) => TaskModel.fromJson(job).pk).toList().join(", ");
            List<Map> tasksList = await databaseHelper.db
                .rawQuery("SELECT * FROM tasks WHERE address_count > 0 AND is_closed_or_not_found = 0 AND pk IN ($tupleTask)");
            if (tasksList.length != tasks.map((job) => TaskModel.fromJson(job).pk).toList().length) {
              List<Map> tasksList = await databaseHelper.db
                  .rawQuery("SELECT * FROM tasks WHERE address_count > 0 AND is_closed_or_not_found = 0 AND pk NOT IN ($tupleTask)");
              if (tasksList.length > 0) {
                textLoader.value = 'Обновляю скрытые задачи';

                for (var oldTask in tasksList) {
                  final task = new TaskModel(
                      pk: oldTask['pk'],
                      city: oldTask['city'],
                      addressCount: oldTask['addressCount'],
                      porchCount: oldTask['porchCount'],
                      type: oldTask['type'],
                      comment: oldTask['comment'],
                      date: oldTask['date'],
                      countImage: oldTask['countImage'],
                      isClosedOrNotFound: true);
                  batch.update("tasks", task.toMap(), where: "pk = ?", whereArgs: [oldTask['pk']]);
                }
              }
              textLoader.value = 'Формирую набор задач';
              tasks.map((job) => TaskModel.fromJson(job)).toList().forEach((task) async {
                List<Map> tasksList = await databaseHelper.db.rawQuery("SELECT * FROM tasks WHERE pk = ?", [
                  task.pk,
                ]);
                if (tasksList.length == 1) {
                  batch.update("tasks", task.toMap(), where: "pk = ?", whereArgs: [task.pk]);
                } else {
                  batch.insert("tasks", task.toMap());
                }
              });
              textLoader.value = 'Формирую набор адресов';
              address.map((job) => AddressModel.fromJson(job)).toList().forEach((a) async {
                List<Map> addressList = await databaseHelper.db.rawQuery("SELECT * FROM address WHERE pk = ? AND porch_count > 0", [
                  a.pk,
                ]);
                if (addressList.length == 1) {
                  batch.update("address", a.toMap(), where: "pk = ? AND porch_count > 0", whereArgs: [a.pk]);
                } else {
                  batch.insert("address", a.toMap());
                }
              });
              textLoader.value = 'Формирую набор подъездов';
              porchs.map((job) => PorchsModel.fromJson(job)).toList().forEach((porch) async {
                List<Map> porchList = await databaseHelper.db
                    .rawQuery("SELECT * FROM porchs WHERE pk = ? AND NOT is_change = 1 AND pkTask = ?", [porch.pk, porch.pkTask]);
                if (porchList.length == 1) {
                  batch.update("porchs", porch.toMap(),
                      where: "pk = ? AND NOT is_change = 1 AND pkTask = ?", whereArgs: [porch.pk, porch.pkTask]);
                } else {
                  batch.insert("porchs", porch.toMap());
                }
              });
              textLoader.value = 'Формирую набор стендов';
              porchs.map((job) => ShieldModel.fromJson(job)).toList().forEach((shields) async {
                List<Map> shieldList = await databaseHelper.db
                    .rawQuery("SELECT * FROM shields WHERE porch_id = ? AND pkTask = ?", [shields.porch_id, shields.pkTask]);
                if (shieldList.length > 0) {
                  batch.update("shields", shields.toMap(),
                      where: "porch_id = ? AND NOT is_send = 1 AND pkTask = ?", whereArgs: [shields.porch_id, shields.pkTask]);
                } else {
                  batch.insert("shields", shields.toMap());
                }
              });

              await batch.commit();
              Get.toNamed('/task');
              Get.lazyPut(() => TaskController());
              TaskController.to.localGetTasks();
            } else {
              Get.snackbar('Новых задач нет', '', snackPosition: SnackPosition.BOTTOM);
              Get.toNamed('/task');
              Get.lazyPut(() => TaskController());
              TaskController.to.localGetTasks();
            }
          } else if (value.statusCode == 204) {
            List<Map> tasksList =
                await databaseHelper.db.rawQuery("SELECT * FROM tasks WHERE address_count > 0 AND is_closed_or_not_found = 0");
            if (tasksList.length > 0) {
              textLoader.value = 'Обновляю скрытые задачи';
              for (var oldTask in tasksList) {
                final task = new TaskModel(
                    pk: oldTask['pk'],
                    city: oldTask['city'],
                    addressCount: oldTask['addressCount'],
                    porchCount: oldTask['porchCount'],
                    type: oldTask['type'],
                    comment: oldTask['comment'],
                    date: oldTask['date'],
                    countImage: oldTask['countImage'],
                    isClosedOrNotFound: true);
                batch.update("tasks", task.toMap(), where: "pk = ?", whereArgs: [oldTask['pk']]);
              }
              await batch.commit();
            }
            Get.toNamed('/task');
            Get.lazyPut(() => TaskController());
            TaskController.to.localGetTasks();
          }
        }).catchError((onError) {});
      }
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
