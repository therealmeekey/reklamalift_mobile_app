import 'package:connectivity/connectivity.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:reklamalift/models/ShieldModel.dart';
import 'package:reklamalift/models/TaskModel.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:reklamalift/http/url.dart';
import 'package:http/http.dart' as http;
import '../main.dart';

class GalleryController extends GetxController {
  static GalleryController get to => Get.find();
  var load = false.obs;
  var data = [].obs;
  var fullImage = 0.obs;
  var countImage = 0.obs;
  var imageSend = 0.obs;
  var step = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  getImageList() async {
    /// 1. Получаем список фотографий из базы данных
    /// 2. Получаем список задач этих фотографий
    /// 3. Формируем список этих задач
    try {
      List<Map> images = await databaseHelper.db.rawQuery("SELECT pkTask FROM shields WHERE img IS NOT ''");
      List tasks = [];
      images.map((job) => ShieldModel.fromJson(job)).toList().forEach((task) async {
        tasks.add(task.pkTask);
      });
      List<Map> list = [];
      for (final pk in tasks.toSet().toList()) {
        List<Map> task = await databaseHelper.db.rawQuery("SELECT * FROM tasks WHERE pk = ?", [pk]);
        List<Map> imageTask = await databaseHelper.db.rawQuery("SELECT pkTask FROM shields WHERE pkTask = ? AND img IS NOT ''", [pk]);
        List<Map> imageSend = await databaseHelper.db.rawQuery("SELECT pkTask FROM shields WHERE pkTask = ? AND is_send = 1", [pk]);
        if (task.length != 0) {
          var withCountImageTasks = {
            'pk': task[0]["pk"],
            'city': task[0]["city"],
            'address_count': task[0]["address_count"],
            'porch_count': task[0]["porch_count"],
            'type': task[0]["type"],
            'comment': task[0]["comment"],
            'date': task[0]["date"],
            'countImage': imageTask.length,
            'imageSend': imageSend.length,
          };
          list.add(withCountImageTasks);
        }
      }
      data.clear();
      data.assignAll(list.map((job) => TaskModel.fromMap(job)).toList());
      data.refresh();
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  sendData() async {
    try {
      load(true);
      imageSend.value = 0;
      countImage.value = 0;
      final connectivityResult = await (Connectivity().checkConnectivity());
      var token = await GetStorage().read('token');
      if (connectivityResult == ConnectivityResult.none) {
        Get.back();
        Get.snackbar('Отсутствует интернет', '', snackPosition: SnackPosition.BOTTOM);
        // return Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) => GalleryItemWidget(pk: pk, city: task[0]['city'], type: task[0]['type'], date: task[0]['date']),
        //         fullscreenDialog: true));
      } else {
        List<Map> shields = await databaseHelper.db.rawQuery("SELECT id FROM shields WHERE img IS NOT ''");
        fullImage.value = shields.length;
        var batch = databaseHelper.db.batch();
        if (shields.length > 0) {
          for (final s in shields) {
            try {
              List<Map> sId = await databaseHelper.db.rawQuery("SELECT * FROM shields WHERE id = ?", [s['id']]);
              step.value = sId[0]['address'].toString() + ', ' + sId[0]['porchNumber'].toString();
              var request = http.MultipartRequest("POST", Uri.parse(API_URL + sendDataUrl));
              request.headers["Authorization"] = "Bearer $token";
              request.fields["pkTask"] = sId[0]['pkTask'].toString();
              request.fields["porch_id"] = sId[0]['porch_id'].toString();
              request.fields["pk_address"] = sId[0]['pk_address'].toString();
              request.fields["broken_shield"] = sId[0]['broken_shield'].toString();
              request.fields["broken_gib"] = sId[0]['broken_gib'].toString();
              request.fields["no_glass"] = sId[0]['no_glass'].toString();
              request.fields["replace_glass"] = sId[0]['replace_glass'].toString();
              request.fields["against_tenants"] = sId[0]['against_tenants'].toString();
              request.fields["no_social_info"] = sId[0]['no_social_info'].toString();
              request.fields["no_top_plank"] = sId[0]['no_top_plank'].toString();
              request.fields["no_left_plank"] = sId[0]['no_left_plank'].toString();
              request.fields["no_right_plank"] = sId[0]['no_right_plank'].toString();
              request.fields["lift_replacing"] = sId[0]['lift_replacing'].toString();
              request.fields["is_repaired"] = sId[0]['is_repaired'].toString();
              request.fields["is_broken"] = sId[0]['is_broken'].toString();
              request.fields["is_display"] = sId[0]['is_display'].toString();
              request.fields["date"] = sId[0]['date'];
              var pic = await http.MultipartFile.fromPath("file_field", sId[0]['img']);
              request.files.add(pic);
              var response = await request.send();
              bool isSend = false;
              if (response.statusCode == 200) {
                isSend = true;
                imageSend += 1;
              }
              final newShield = new ShieldModel(
                pkTask: sId[0]['pkTask'].toString(),
                is_send: isSend,
                pk_address: sId[0]['pk_address'],
                porch_id: sId[0]['porch_id'],
                broken_shield: sId[0]['broken_shield'] == 1 ? true : false,
                broken_gib: sId[0]['broken_gib'] == 1 ? true : false,
                no_glass: sId[0]['no_glass'] == 1 ? true : false,
                replace_glass: sId[0]['replace_glass'] == 1 ? true : false,
                against_tenants: sId[0]['against_tenants'] == 1 ? true : false,
                no_social_info: sId[0]['no_social_info'] == 1 ? true : false,
                no_top_plank: sId[0]['no_top_plank'] == 1 ? true : false,
                no_left_plank: sId[0]['no_left_plank'] == 1 ? true : false,
                no_right_plank: sId[0]['no_right_plank'] == 1 ? true : false,
                lift_replacing: sId[0]['lift_replacing'] == 1 ? true : false,
                is_repaired: sId[0]['is_repaired'] == 1 ? true : false,
                is_display: sId[0]['is_display'] == 1 ? true : false,
                address: sId[0]['address'],
                porchNumber: sId[0]['porchNumber'],
                img: sId[0]['img'],
                date: sId[0]['date'],
                is_broken: sId[0]['is_broken'] == 1 ? true : false,
              );
              batch.update("shields", newShield.toMap(), where: "id = ?", whereArgs: [sId[0]['id']]);
              await batch.commit();
            } catch (exception, stackTrace) {
              print(exception);
              print(stackTrace);
              await Sentry.captureException(
                exception,
                stackTrace: stackTrace,
              );
            }
            countImage += 1;
          }
          Get.snackbar('Выгрузка фотографий закончена.', 'Количество - $imageSend.', snackPosition: SnackPosition.BOTTOM);
          Get.back();
          getImageList();
        }
      }
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
