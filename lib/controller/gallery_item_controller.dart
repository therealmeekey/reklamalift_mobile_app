import 'dart:io' show File;
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:reklamalift/http/url.dart';
import 'package:get_storage/get_storage.dart';
import 'package:reklamalift/controller/camera_controller.dart';
import 'package:reklamalift/controller/porch_controller.dart';
import 'package:reklamalift/controller/shield_status_controller.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:reklamalift/models/PhotoModel.dart';
import 'package:reklamalift/models/ShieldModel.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:http/http.dart' as http;
import '../main.dart';
import 'address_controller.dart';
import 'gallery_controller.dart';

class GalleryItemController extends GetxController {
  static GalleryItemController get to => Get.find();
  var taskIndex = 0.obs;
  var load = false.obs;
  var data = [].obs;
  var fullImage = 0.obs;
  var countImage = 0.obs;
  var imageSend = 0.obs;
  var step = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  getPhotos() async {
    /// 1. Получаем список фотографий по конкретной задаче
    /// 2. Формируем список этих фотографий
    try {
      List<Map> images = await databaseHelper.db.rawQuery(
          "SELECT id, address, porchNumber, porch_id, is_send FROM shields WHERE pkTask = ? AND img IS NOT ''",
          [GalleryController.to.data[taskIndex.value].pk]);
      List list = [];
      for (var i in images) {
        bool isBroken = i['is_broken'] == 1 ? true : false;
        int id = i['id'];
        String image = i['img'];
        String address = i['address'];
        String porchNumber = i['porchNumber'];
        int porchId = i['porch_id'];
        String status;
        if (i['is_send'] == 1) {
          status = 'Отправлено';
        } else {
          status = 'Не отправленно';
        }
        list.add(
            {'id': id, 'image': image, 'title': '$address, $porchNumber', 'subtitle': status, 'porchId': porchId, 'isBroken': isBroken});
      }
      data.clear();
      data.assignAll(list.map((job) => PhotoModel.fromMap(job)).toList().reversed.toList());
      data.refresh();
    } catch (exception, stackTrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  Future<Widget> getImage(id) async {
    try {
      List<Map> images = await databaseHelper.db.rawQuery("SELECT img FROM shields WHERE id = ?", [id]);
      return Image.file(File(images[0]['img']));
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  Future editPorch(porchId, idImage) async {
    try {
      Get.dialog(AlertDialog(
        title: Text('Изменить фотографию?'),
        content: FutureBuilder<Widget>(
            future: getImage(idImage),
            builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
              if (snapshot.hasData) {
                return snapshot.data;
              } else {
                return Container(child: CircularProgressIndicator());
              }
            }),
        actions: [
          TextButton(
            child: Text(
              "Изменить",
              style: actionButtonText,
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(orange),
            ),
            onPressed: () async {
              //TODO: На потом, если есть две фото, то на этапе присваивания porchIndex может быть баг
              Get.back();
              List<Map> porch = await databaseHelper.db.rawQuery("SELECT pk_address FROM porchs WHERE porch_id = ? AND pkTask = ?",
                  [porchId, GalleryController.to.data[taskIndex.value].pk.toString()]);

              /// Получаю индекс элемента задачи в списке и присваиваю его переменной в AddressController
              AddressController.to.taskIndex.value =
                  TaskController.to.data.indexWhere((i) => i.pk == GalleryController.to.data[taskIndex.value].pk).toString();
              await AddressController.to.getAddressList();

              /// Получаю индекс элемента адреса в списке и присваиваю его переменной в PorchController
              PorchController.to.addressIndex.value =
                  AddressController.to.data.indexWhere((i) => i.pk == porch[0]['pk_address']).toString();
              await PorchController.to.getPorchsList();

              /// Получаю индекс элемента подъезда в списке и присваиваю его переменной в ShieldStatusController
              ShieldStatusController.to.porchIndex.value = PorchController.to.data.indexWhere((i) => i.porchId == porchId).toString();
              await ShieldStatusController.to.getShieldStatusList();
              Get.lazyPut(() => CameraController());
              CameraController.to.idImage.value = idImage;
              CameraController.to.isEditPorch.value = true;
              CameraController.to.isDoubleStand.value = false;
              CameraController.to.needRepair.value = false;
              Get.toNamed('/shield_status');
            },
          ),
          TextButton(
            child: Text(
              "Нет",
              style: actionButtonText,
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(grey),
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ],
      ));
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  editPorchIsBroken(idImage) async {
    try {
      Get.dialog(AlertDialog(
        title: Text('Просмотр фотографии'),
        content: FutureBuilder<Widget>(
            future: getImage(idImage),
            builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
              if (snapshot.hasData) {
                return snapshot.data;
              } else {
                return Container(child: CircularProgressIndicator());
              }
            }),
        actions: [
          TextButton(
            child: Text(
              "Закрыть",
              style: actionButtonText,
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(grey),
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ],
      ));
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  removeData() async {
    /// Удаляем данные shields и перенаправляем на GalleryView
    try {
      load(true);
      await databaseHelper.db.delete("shields", where: "pkTask = ${GalleryController.to.data[taskIndex.value].pk} AND is_send = 1");
      Get.offAndToNamed('/gallery');
      Get.lazyPut(() => GalleryController());
      GalleryController.to.getImageList();
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  getColor(text) {
    if (text == 'Отправлено') {
      return Colors.green;
    } else {
      return Colors.red;
    }
  }

  sendData(pk, isFull) async {
    try {
      load(true);
      imageSend.value = 0;
      countImage.value = 0;
      final connectivityResult = await (Connectivity().checkConnectivity());
      var token = await GetStorage().read('token');
      if (connectivityResult == ConnectivityResult.none) {
        Get.back();
        Get.snackbar('Отсутствует интернет', '', snackPosition: SnackPosition.BOTTOM);
        // return Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) => GalleryItemWidget(pk: pk, city: task[0]['city'], type: task[0]['type'], date: task[0]['date']),
        //         fullscreenDialog: true));
      } else {
        List<Map> shields;
        if (isFull) {
          shields = await databaseHelper.db.rawQuery("SELECT id FROM shields WHERE pkTask = ? AND img IS NOT ''", [pk.toString()]);
        } else {
          shields = await databaseHelper.db
              .rawQuery("SELECT id FROM shields WHERE pkTask = ? AND img IS NOT '' AND is_send = 0", [pk.toString()]);
        }
        fullImage.value = shields.length;
        var batch = databaseHelper.db.batch();
        if (shields.length > 0) {
          for (final s in shields) {
            try {
              List<Map> sId = await databaseHelper.db.rawQuery("SELECT * FROM shields WHERE id = ?", [s['id']]);
              step.value = sId[0]['address'].toString() + ', ' + sId[0]['porchNumber'].toString();
              var request = http.MultipartRequest("POST", Uri.parse(API_URL + sendDataUrl));
              request.headers["Authorization"] = "Bearer $token";
              request.fields["pkTask"] = sId[0]['pkTask'].toString();
              request.fields["porch_id"] = sId[0]['porch_id'].toString();
              request.fields["pk_address"] = sId[0]['pk_address'].toString();
              request.fields["broken_shield"] = sId[0]['broken_shield'].toString();
              request.fields["broken_gib"] = sId[0]['broken_gib'].toString();
              request.fields["no_glass"] = sId[0]['no_glass'].toString();
              request.fields["replace_glass"] = sId[0]['replace_glass'].toString();
              request.fields["against_tenants"] = sId[0]['against_tenants'].toString();
              request.fields["no_social_info"] = sId[0]['no_social_info'].toString();
              request.fields["no_top_plank"] = sId[0]['no_top_plank'].toString();
              request.fields["no_left_plank"] = sId[0]['no_left_plank'].toString();
              request.fields["no_right_plank"] = sId[0]['no_right_plank'].toString();
              request.fields["lift_replacing"] = sId[0]['lift_replacing'].toString();
              request.fields["is_repaired"] = sId[0]['is_repaired'].toString();
              request.fields["is_broken"] = sId[0]['is_broken'].toString();
              request.fields["is_display"] = sId[0]['is_display'].toString();
              request.fields["date"] = sId[0]['date'];
              var pic = await http.MultipartFile.fromPath("file_field", sId[0]['img']);
              request.files.add(pic);
              var response = await request.send();
              bool isSend = false;
              if (response.statusCode == 200) {
                isSend = true;
                imageSend += 1;
              }
              final newShield = new ShieldModel(
                pkTask: sId[0]['pkTask'].toString(),
                is_send: isSend,
                pk_address: sId[0]['pk_address'],
                porch_id: sId[0]['porch_id'],
                broken_shield: sId[0]['broken_shield'] == 1 ? true : false,
                broken_gib: sId[0]['broken_gib'] == 1 ? true : false,
                no_glass: sId[0]['no_glass'] == 1 ? true : false,
                replace_glass: sId[0]['replace_glass'] == 1 ? true : false,
                against_tenants: sId[0]['against_tenants'] == 1 ? true : false,
                no_social_info: sId[0]['no_social_info'] == 1 ? true : false,
                no_top_plank: sId[0]['no_top_plank'] == 1 ? true : false,
                no_left_plank: sId[0]['no_left_plank'] == 1 ? true : false,
                no_right_plank: sId[0]['no_right_plank'] == 1 ? true : false,
                lift_replacing: sId[0]['lift_replacing'] == 1 ? true : false,
                is_repaired: sId[0]['is_repaired'] == 1 ? true : false,
                is_display: sId[0]['is_display'] == 1 ? true : false,
                address: sId[0]['address'],
                porchNumber: sId[0]['porchNumber'],
                img: sId[0]['img'],
                date: sId[0]['date'],
                is_broken: sId[0]['is_broken'] == 1 ? true : false,
              );
              batch.update("shields", newShield.toMap(), where: "id = ?", whereArgs: [sId[0]['id']]);
              await batch.commit();
            } catch (exception, stackTrace) {
              print(exception);
              print(stackTrace);
              await Sentry.captureException(
                exception,
                stackTrace: stackTrace,
              );
            }
            countImage += 1;
          }
          Get.snackbar('Выгрузка фотографий закончена.', 'Количество - $imageSend.', snackPosition: SnackPosition.BOTTOM);
          Get.back();
          getPhotos();
        }
      }
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
