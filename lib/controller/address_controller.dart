import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:reklamalift/models/AddressModel.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import '../main.dart';

class AddressController extends GetxController {
  static AddressController get to => Get.find();
  var load = false.obs;
  var data = [].obs;
  var taskIndex = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  getAddressList() async {
    /// Выгружаем данные из БД
    try {
      load(true);
      List<Map> addressList = await databaseHelper.db.rawQuery("SELECT * FROM address WHERE pk_task = ? AND porch_count > 0", [
        TaskController.to.data[int.parse(taskIndex.value)].pk.toString(),
      ]);
      if (addressList.length == 0) {
        Get.dialog(AlertDialog(
          title: Text('Адреса не найдены'),
          content: Text('Попробуйте перезагрузить приложение'),
          actions: [
            TextButton(
              child: Text(
                "Закрыть",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                Get.back();
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.grey),
              ),
            ),
          ],
        ));
        return Get.offAndToNamed('/task');
      }
      List addressListSuccess = [];
      for (var address in addressList) {
        List<Map> porchList = await databaseHelper.db.rawQuery(
            "SELECT id FROM porchs WHERE pk_address = ? AND pkTask = ? AND NOT is_change = 1",
            [address['pk'], TaskController.to.data[int.parse(taskIndex.value)].pk.toString()]);
        if (porchList.length > 0) {
          addressListSuccess.add(address);
        }
      }
      data.clear();
      data.assignAll(addressListSuccess.map((job) => AddressModel.fromMap(job)).toList());
      data.refresh();
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
