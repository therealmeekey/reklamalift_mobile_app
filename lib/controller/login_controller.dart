import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:reklamalift/http/url.dart';
import 'package:reklamalift/http/request.dart';
import 'package:reklamalift/utility/storage.dart';
import 'package:reklamalift/utility/text_style.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class LoginController extends GetxController {
  static LoginController get to => Get.find();
  var load = false.obs;
  TextEditingController emailTextController;
  TextEditingController passwordController;

  @override
  void onInit() {
    emailTextController = TextEditingController();
    passwordController = TextEditingController();
    _checkEmail();
    super.onInit();
  }

  void _checkEmail() async {
    final email = await GetStorage().read('email');
    if (email == null || email.toString() == '') {
      emailTextController.value = TextEditingValue(
          text: '',
          selection: TextSelection.fromPosition(
            TextPosition(offset: 0),
          ));
    } else {
      emailTextController.value = TextEditingValue(
          text: email.toString(),
          selection: TextSelection.fromPosition(
            TextPosition(offset: email.toString().length),
          ));
    }
  }

  void loginRequest() async {
    try {
      load(true);
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        Get.dialog(AlertDialog(
          title: Text('Проблемы с интернетом'),
          content: Text('Проверьте включен ли у вас интернет'),
          actions: [
            TextButton(
              child: Text(
                "Закрыть",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.grey),
              ),
              onPressed: () {
                load(true);
                Get.back();
              },
            ),
          ],
        ));
      } else {
        Request request = Request(url: login, body: {
          'email': emailTextController.text,
          'password': passwordController.text
        });
        await request.post().then((value) {
          if (value.statusCode == 200) {
            final responseJson = json.decode(value.body);
            saveLoginDate(responseJson);
            Get.toNamed('/splash');
          } else {
            final responseError = json.decode(utf8.decode(value.bodyBytes));
            Get.dialog(AlertDialog(
              title: Text('Ошибка авторизации'),
              content: Text('${responseError.toString()}'),
              actions: [
                TextButton(
                  child: Text(
                    "Закрыть",
                    style: actionButtonText,
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.grey),
                  ),
                  onPressed: () {
                    load(true);
                    Get.back();
                  },
                ),
              ],
            ));
          }
        }).catchError((onError) {
          print(onError);
        });
      }
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  logout() async {
    /// Удаляем токен и перенаправляем на LoginView
    try {
      await GetStorage().remove('token');
      Get.toNamed('/login');
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  @override
  void onClose() {
    emailTextController?.dispose();
    passwordController?.dispose();
    super.onClose();
  }
}
