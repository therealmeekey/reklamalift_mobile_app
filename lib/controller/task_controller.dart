import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:reklamalift/controller/splash_controller.dart';
import 'package:reklamalift/http/url.dart';
import 'package:reklamalift/models/AddressModel.dart';
import 'package:reklamalift/models/PorchsModel.dart';
import 'package:reklamalift/models/ShieldModel.dart';
import 'package:reklamalift/models/TaskModel.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:reklamalift/http/request.dart';
import '../main.dart';

class TaskController extends GetxController {
  static TaskController get to => Get.find();
  var textLoader = ''.obs;
  var load = false.obs;
  var errorText = ''.obs;
  TextEditingController passwordController;
  var data = [].obs;

  @override
  void onInit() {
    passwordController = TextEditingController();
    localGetTasks();
    super.onInit();
  }

  localGetTasks() async {
    try {
      load(true);
      List<Map> list = await databaseHelper.db.rawQuery("SELECT * FROM tasks WHERE address_count > 0 AND is_closed_or_not_found = 0");
      data.clear();
      data.assignAll(list.map((job) => TaskModel.fromMap(job)).toList());
      data.refresh();
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  updateData() async {
    /// Если нет интернет соединения:
    /// Выгружаем данные из БД
    ///
    /// Если есть интернет соединение:
    /// 1. Запрашиваем данные с reklamalift.com
    /// 2. Смотрим есть ли у нас данные под этим pk в бд
    /// 3. Если есть удаляем их и загружаем по новой
    /// 4. Если нет записываем вновь полученные данные
    try {
      load(true);
      var token = await GetStorage().read('token');
      final connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        List<Map> list = await databaseHelper.db.rawQuery("SELECT * FROM tasks WHERE address_count > 0 AND is_closed_or_not_found = 0");
        data.clear();
        data.assignAll(list.map((job) => TaskModel.fromMap(job)).toList().toSet().toList());
        data.refresh();
      } else {
        Request request = Request(url: getData, headers: {"Authorization": "Bearer $token"});
        var batch = databaseHelper.db.batch();
        await request.get().then((value) async {
          if (value.statusCode == 200) {
            /// Если есть активные задачи на RL
            List tasks = json.decode(utf8.decode(value.bodyBytes))[0]['tasks_list'];
            List address = json.decode(utf8.decode(value.bodyBytes))[0]['address_list'];
            List porchs = json.decode(utf8.decode(value.bodyBytes))[0]['porch_list'];

            var tupleTask = tasks.map((job) => TaskModel.fromJson(job).pk).toList().join(", ");
            List<Map> tasksList = await databaseHelper.db
                .rawQuery("SELECT * FROM tasks WHERE address_count > 0 AND is_closed_or_not_found = 0 AND pk NOT IN ($tupleTask)");
            if (tasksList.length > 0) {
              /// Если есть закрытые или выполненные задачи в БД
              for (var oldTask in tasksList) {
                final task = new TaskModel(
                    pk: oldTask['pk'],
                    city: oldTask['city'],
                    addressCount: oldTask['addressCount'],
                    porchCount: oldTask['porchCount'],
                    type: oldTask['type'],
                    comment: oldTask['comment'],
                    date: oldTask['date'],
                    countImage: oldTask['countImage'],
                    isClosedOrNotFound: true);
                batch.update("tasks", task.toMap(), where: "pk = ?", whereArgs: [oldTask['pk']]);
              }
            }

            /// Обновляю задачи
            tasks.map((job) => TaskModel.fromJson(job)).toList().forEach((task) async {
              List<Map> tasksList = await databaseHelper.db.rawQuery("SELECT * FROM tasks WHERE pk = ?", [
                task.pk,
              ]);
              if (tasksList.length == 1) {
                batch.update("tasks", task.toMap(), where: "pk = ?", whereArgs: [task.pk]);
              } else {
                batch.insert("tasks", task.toMap());
              }
            });

            /// Обновляю Адреса
            address.map((job) => AddressModel.fromJson(job)).toList().forEach((a) async {
              List<Map> addressList = await databaseHelper.db.rawQuery("SELECT * FROM address WHERE pk = ? AND porch_count > 0", [
                a.pk,
              ]);
              if (addressList.length == 1) {
                batch.update("address", a.toMap(), where: "pk = ? AND porch_count > 0", whereArgs: [a.pk]);
              } else {
                batch.insert("address", a.toMap());
              }
            });

            /// Обновляю подъезды
            porchs.map((job) => PorchsModel.fromJson(job)).toList().forEach((porch) async {
              List<Map> porchList = await databaseHelper.db.rawQuery("SELECT * FROM porchs WHERE pk = ? AND NOT is_change = 1", [
                porch.pk,
              ]);
              if (porchList.length == 1) {
                batch.update("porchs", porch.toMap(), where: "pk = ? AND NOT is_change = 1", whereArgs: [porch.pk]);
              } else {
                batch.insert("porchs", porch.toMap());
              }
            });

            /// Обновляю стенды
            porchs.map((job) => ShieldModel.fromJson(job)).toList().forEach((shields) async {
              List<Map> shieldList = await databaseHelper.db
                  .rawQuery("SELECT * FROM shields WHERE porch_id = ? AND pkTask = ?", [shields.porch_id, shields.pkTask]);
              if (shieldList.length > 0) {
                batch.update("shields", shields.toMap(),
                    where: "porch_id = ? AND NOT is_send = 1 AND pkTask = ? AND img = ''", whereArgs: [shields.porch_id, shields.pkTask]);
              } else {
                batch.insert("shields", shields.toMap());
              }
            });
            await batch.commit();
            localGetTasks();
          } else if (value.statusCode == 204) {
            /// Если нет активных задач на RL
            List<Map> tasksList =
                await databaseHelper.db.rawQuery("SELECT * FROM tasks WHERE address_count > 0 AND is_closed_or_not_found = 0");
            if (tasksList.length > 0) {
              for (var oldTask in tasksList) {
                final task = new TaskModel(
                    pk: oldTask['pk'],
                    city: oldTask['city'],
                    addressCount: oldTask['addressCount'],
                    porchCount: oldTask['porchCount'],
                    type: oldTask['type'],
                    comment: oldTask['comment'],
                    date: oldTask['date'],
                    countImage: oldTask['countImage'],
                    isClosedOrNotFound: true);
                batch.update("tasks", task.toMap(), where: "pk = ?", whereArgs: [oldTask['pk']]);
              }
            }
            await batch.commit();
            Get.dialog(AlertDialog(
              title: Text('Задачи отсутствуют'),
              content: Text('Задачи не найдены!Обратитесь к техническому руководителю вашей компании для уточнения'),
              actions: [
                TextButton(
                    child: Text(
                      "Ок",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(Colors.orange),
                    ),
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ));
          }
        }).catchError((onError) {
          print(onError);
        });
      }
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  clearRAM() async {
    /// Очищаем память приложения
    try {
      load(true);
      Get.back();
      var token = await GetStorage().read('token');
      Request request = Request(url: checkPswd, headers: {"Authorization": "Bearer $token"}, body: {'password': passwordController.text});
      await request.post().then((value) async {
        passwordController.clear();
        if (value.statusCode == 200) {
          await databaseHelper.db.delete("tasks");
          await databaseHelper.db.delete("address");
          await databaseHelper.db.delete("porchs");
          await databaseHelper.db.delete("shields");
          Get.offAndToNamed('/splash');
          SplashController.to.getAllData();
          Get.snackbar('Память приложения очищена', '', snackPosition: SnackPosition.BOTTOM);
        } else if (value.statusCode == 500) {
          Get.snackbar('Пароль не верный', '', snackPosition: SnackPosition.BOTTOM);
        }
      }).catchError((onError) {
        print(onError);
      });
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  @override
  void onClose() {
    passwordController?.dispose();
    super.onClose();
  }
}
