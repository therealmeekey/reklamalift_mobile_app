import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:reklamalift/controller/porch_controller.dart';
import 'package:reklamalift/models/ShieldModel.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import '../main.dart';

class ShieldStatusController extends GetxController {
  static ShieldStatusController get to => Get.find();
  var load = false.obs;
  var data = [].obs;
  var shieldDb = [].obs;
  var porchIndex = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  getShieldStatusList() async {
    try {
      List<Map> list = await databaseHelper.db.rawQuery("SELECT * FROM shields WHERE porch_id = ? and pkTask = ?", [
        PorchController.to.data[int.parse(porchIndex.value)].porchId,
        PorchController.to.data[int.parse(porchIndex.value)].pkTask.toString()
      ]);
      var index;
      if (list.length > 1) {
        index = 1;
      } else {
        index = 0;
      }
      List<Map> shields = [];

      /// broken_shield and no_screen
      if (toBoolean(list[index]['broken_shield'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Рамка на месте');
        } else {
          data.update("name", (value) => 'Экран на месте');
        }
        data.update("value", (value) => toBoolean(list[index]['broken_shield'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Отсутствует рамка');
        } else {
          data.update("name", (value) => 'Отсутствует экран');
        }
        data.update("value", (value) => toBoolean(list[index]['broken_shield'].toString()));
        shields.add(data);
      }

      /// broken_gib and screen_not_working
      if (toBoolean(list[index]['broken_gib'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Уголок на месте');
        } else {
          data.update("name", (value) => 'Экран работает');
        }
        data.update("value", (value) => toBoolean(list[index]['broken_gib'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Отсутствует уголок');
        } else {
          data.update("name", (value) => 'Экран не работает');
        }
        data.update("value", (value) => toBoolean(list[index]['broken_gib'].toString()));
        shields.add(data);
      }

      /// against_tenants and no_bottom_plank
      if (toBoolean(list[index]['against_tenants'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Нижняя планка на месте');
        } else {
          data.update("name", (value) => 'Нижняя плашка на месте');
        }
        data.update("value", (value) => toBoolean(list[index]['against_tenants'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Отсутствует нижняя планка');
        } else {
          data.update("name", (value) => 'Отсутствует нижняя плашка');
        }
        data.update("value", (value) => toBoolean(list[index]['against_tenants'].toString()));
        shields.add(data);
      }

      /// no_glass and no_electricity
      if (toBoolean(list[index]['no_glass'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Защитное стекло на месте');
        } else {
          data.update("name", (value) => 'Есть электричества в ЖК');
        }
        data.update("value", (value) => toBoolean(list[index]['no_glass'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Отсутствует защитное стекло');
        } else {
          data.update("name", (value) => 'Нет электричества в ЖК');
        }
        data.update("value", (value) => toBoolean(list[index]['no_glass'].toString()));
        shields.add(data);
      }

      /// replace_glass and screen_damaged
      if (toBoolean(list[index]['replace_glass'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Защитное стекло целое');
        } else {
          data.update("name", (value) => 'Экран цел');
        }
        data.update("value", (value) => toBoolean(list[index]['replace_glass'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Заменить защитное стекло');
        } else {
          data.update("name", (value) => 'Экран поврежден');
        }
        data.update("value", (value) => toBoolean(list[index]['replace_glass'].toString()));
        shields.add(data);
      }

      /// no_social_info and no_bottom_bolts
      if (toBoolean(list[index]['no_social_info'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Лифт не сломан');
        } else {
          data.update("name", (value) => 'Нижние болты на месте');
        }
        data.update("value", (value) => toBoolean(list[index]['no_social_info'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Лифт сломан');
        } else {
          data.update("name", (value) => 'Отсутствуют нижние болты');
        }
        data.update("value", (value) => toBoolean(list[index]['no_social_info'].toString()));
        shields.add(data);
      }

      /// lift_replacing and content_frozen
      if (toBoolean(list[index]['lift_replacing'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Лифт на месте');
        } else {
          data.update("name", (value) => 'Контент транслируется');
        }
        data.update("value", (value) => toBoolean(list[index]['lift_replacing'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Замена лифта');
        } else {
          data.update("name", (value) => 'Контент завис');
        }
        data.update("value", (value) => toBoolean(list[index]['lift_replacing'].toString()));
        shields.add(data);
      }

      /// no_top_plank and no_top_plank
      if (toBoolean(list[index]['no_top_plank'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Верхняя планка на месте');
        } else {
          data.update("name", (value) => 'Верхняя плашка на месте');
        }
        data.update("value", (value) => toBoolean(list[index]['no_top_plank'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Отсутствует верхняя планка');
        } else {
          data.update("name", (value) => 'Отсутствует верхняя плашка');
        }
        data.update("value", (value) => toBoolean(list[index]['no_top_plank'].toString()));
        shields.add(data);
      }

      /// no_left_plank and no_top_bolts
      if (toBoolean(list[index]['no_left_plank'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Левая боковая планка на месте');
        } else {
          data.update("name", (value) => 'Верхние болты на месте');
        }
        data.update("value", (value) => toBoolean(list[index]['no_left_plank'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Отсутствует боковая планка левая');
        } else {
          data.update("name", (value) => 'Отсутствуют верхние болты');
        }
        data.update("value", (value) => toBoolean(list[index]['no_left_plank'].toString()));
        shields.add(data);
      }

      /// no_right_plank and no_sdcard
      if (toBoolean(list[index]['no_right_plank'].toString()) == false) {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Правая боковая планка на месте');
        } else {
          data.update("name", (value) => 'Флешка на месте');
        }
        data.update("value", (value) => toBoolean(list[index]['no_right_plank'].toString()));
        shields.add(data);
      } else {
        Map data = {
          'name': "",
          'value': false,
        };
        if (toBoolean(list[index]['is_display'].toString()) == false) {
          data.update("name", (value) => 'Отсутствует боковая планка правая');
        } else {
          data.update("name", (value) => 'Отсутствует флешка');
        }
        data.update("value", (value) => toBoolean(list[index]['no_right_plank'].toString()));
        shields.add(data);
      }
      data.clear();
      data.assignAll(shields);
      data.refresh();
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  changeValue(name, value) async {
    try {
      if (value) {
        value = false;
      } else {
        value = true;
      }
      List<Map> list = await databaseHelper.db.rawQuery("SELECT * FROM shields WHERE porch_id = ? AND pkTask = ?", [
        PorchController.to.data[int.parse(porchIndex.value)].porchId,
        PorchController.to.data[int.parse(porchIndex.value)].pkTask.toString()
      ]);
      var brokenShield = toBoolean(list[0]['broken_shield'].toString());
      var brokenGib = toBoolean(list[0]['broken_gib'].toString());
      var noGlass = toBoolean(list[0]['no_glass'].toString());
      var replaceGlass = toBoolean(list[0]['replace_glass'].toString());
      var againstTenants = toBoolean(list[0]['against_tenants'].toString());
      var noSocialInfo = toBoolean(list[0]['no_social_info'].toString());
      var noTopPlank = toBoolean(list[0]['no_top_plank'].toString());
      var noLeftPlank = toBoolean(list[0]['no_left_plank'].toString());
      var noRightPlank = toBoolean(list[0]['no_right_plank'].toString());
      var liftReplacing = toBoolean(list[0]['lift_replacing'].toString());
      if (name == 'Лифт на месте' || name == 'Замена лифта' || name == 'Контент транслируется' || name == 'Контент завис') {
        liftReplacing = value;
      } else if (name == 'Лифт не сломан' ||
          name == 'Лифт сломан' ||
          name == 'Нижние болты на месте' ||
          name == 'Отсутствуют нижние болты') {
        noSocialInfo = value;
      } else if (name == 'Рамка на месте' || name == 'Отсутствует рамка' || name == 'Экран на месте' || name == 'Отсутствует экран') {
        brokenShield = value;
      } else if (name == 'Уголок на месте' || name == 'Отсутствует уголок' || name == 'Экран работает' || name == 'Экран не работает') {
        brokenGib = value;
      } else if (name == 'Защитное стекло на месте' ||
          name == 'Отсутствует защитное стекло' ||
          name == 'Есть электричества в ЖК' ||
          name == 'Нет электричества в ЖК') {
        noGlass = value;
      } else if (name == 'Защитное стекло целое' ||
          name == 'Заменить защитное стекло' ||
          name == 'Экран цел' ||
          name == 'Экран поврежден') {
        replaceGlass = value;
      } else if (name == 'Нижняя планка на месте' ||
          name == 'Отсутствует нижняя планка' ||
          name == 'Нижняя плашка на месте' ||
          name == 'Отсутствует нижняя плашка') {
        againstTenants = value;
      } else if (name == 'Верхняя планка на месте' ||
          name == 'Отсутствует верхняя планка' ||
          name == 'Верхняя плашка на месте' ||
          name == 'Отсутствует верхняя плашка') {
        noTopPlank = value;
      } else if (name == 'Левая боковая планка на месте' ||
          name == 'Отсутствует боковая планка левая' ||
          name == 'Верхние болты на месте' ||
          name == 'Отсутствуют верхние болты') {
        noLeftPlank = value;
      } else if (name == 'Правая боковая планка на месте' ||
          name == 'Отсутствует боковая планка правая' ||
          name == 'Флешка на месте' ||
          name == 'Отсутствует флешка') {
        noRightPlank = value;
      }
      var shields = [
        {
          'pkTask': PorchController.to.data[int.parse(porchIndex.value)].pkTask.toString(),
          'is_send': false,
          'pk_address': PorchController.to.data[int.parse(porchIndex.value)].pkAddress,
          'porch_id': PorchController.to.data[int.parse(porchIndex.value)].porchId,
          'broken_shield': brokenShield,
          'broken_gib': brokenGib,
          'no_glass': noGlass,
          'replace_glass': replaceGlass,
          'against_tenants': againstTenants,
          'no_social_info': noSocialInfo,
          'no_top_plank': noTopPlank,
          'no_left_plank': noLeftPlank,
          'no_right_plank': noRightPlank,
          'lift_replacing': liftReplacing,
          'is_repaired': false,
          'is_display': list[0]['is_display'] == 1 ? true : false
        }
      ];
      shields.map((job) => ShieldModel.fromJson(job)).toList().forEach((shield) async {
        await databaseHelper.db.update("shields", shield.toMap(), where: "porch_id = ? AND pkTask = ?", whereArgs: [
          PorchController.to.data[int.parse(porchIndex.value)].porchId,
          PorchController.to.data[int.parse(porchIndex.value)].pkTask.toString()
        ]);
      });
      await getShieldStatusList();
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  saveData() async {
    try {
      load(true);
      List<Map> list = await databaseHelper.db.rawQuery("SELECT * FROM shields WHERE porch_id = ? AND pkTask = ?", [
        PorchController.to.data[int.parse(porchIndex.value)].porchId,
        PorchController.to.data[int.parse(porchIndex.value)].pkTask.toString()
      ]);
      List shields = [
        {
          'pkTask': PorchController.to.data[int.parse(porchIndex.value)].pkTask.toString(),
          'is_send': false,
          'pk_address': PorchController.to.data[int.parse(porchIndex.value)].pkAddress,
          'porch_id': PorchController.to.data[int.parse(porchIndex.value)].porchId,
          'broken_shield': toBoolean(list[0]['broken_shield'].toString()),
          'broken_gib': toBoolean(list[0]['broken_gib'].toString()),
          'no_glass': toBoolean(list[0]['no_glass'].toString()),
          'replace_glass': toBoolean(list[0]['replace_glass'].toString()),
          'against_tenants': toBoolean(list[0]['against_tenants'].toString()),
          'no_social_info': toBoolean(list[0]['no_social_info'].toString()),
          'no_top_plank': toBoolean(list[0]['no_top_plank'].toString()),
          'no_left_plank': toBoolean(list[0]['no_left_plank'].toString()),
          'no_right_plank': toBoolean(list[0]['no_right_plank'].toString()),
          'lift_replacing': toBoolean(list[0]['lift_replacing'].toString()),
          'is_repaired': false,
          'is_display': list[0]['is_display'] == 1 ? true : false,
        }
      ];
      shieldDb.clear();
      shieldDb.add(shields[0]);
      shieldDb.refresh();
      shields.map((job) => ShieldModel.fromJson(job)).toList().forEach((shield) async {
        await databaseHelper.db.update("shields", shield.toMap(), where: "porch_id = ? AND pkTask = ?", whereArgs: [
          PorchController.to.data[int.parse(porchIndex.value)].porchId,
          PorchController.to.data[int.parse(porchIndex.value)].pkTask.toString()
        ]);
      });
      Get.toNamed('/camera');
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  bool toBoolean(String str, [bool strict]) {
    if (strict == true) {
      return str == '1' || str == 'true';
    }
    return str != '0' && str != 'false' && str != '';
  }

  getColor(value) {
    if (value == false) {
      return Colors.green;
    } else {
      return Colors.red;
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
