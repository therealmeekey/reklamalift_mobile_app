import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:reklamalift/controller/address_controller.dart';
import 'package:reklamalift/controller/porch_controller.dart';
import 'package:reklamalift/controller/shield_status_controller.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:reklamalift/models/AddressModel.dart';
import 'package:reklamalift/models/PorchsModel.dart';
import 'package:reklamalift/models/ShieldModel.dart';
import 'package:reklamalift/models/TaskModel.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';
import 'package:reklamalift/view/camera_view.dart';
import '../main.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class CameraController extends GetxController {
  static CameraController get to => Get.find();
  var load = false.obs;
  var data = [].obs;
  var needRepair = false.obs;
  var isRepaired = false.obs;
  var isDoubleStand = false.obs;
  var isEditPorch = false.obs;
  var idImage = 0.obs;
  var textLoader = ''.obs;
  var imageFile = ''.obs;
  bool result = true;

  @override
  void onInit() {
    super.onInit();
  }

  upload() async {
    try {
      load(true);
      if (File(imageFile.value) == null) {
        Get.toNamed('/shield_status');
        Get.snackbar('Фотография не найдена', 'Повторите фото', snackPosition: SnackPosition.BOTTOM);
      }
      List<Map> tasks = await databaseHelper.db.rawQuery("Select * from tasks where pk = ?", [
        TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].pk,
      ]);
      List<Map> address = await databaseHelper.db.rawQuery("Select porch_count from address where pk = ? and pk_task = ?", [
        AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].pk,
        TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].pk
      ]);
      int porchCountTasks = tasks[0]['porch_count'];
      int addressCountTasks = tasks[0]['address_count'];
      int porchCountAddress = address[0]['porch_count'];

      /// Ремонт на месте
      textLoader.value = 'Смотрю необходимость в ремонте на месте';
      List<Map> shields = await databaseHelper.db.rawQuery("SELECT * FROM shields WHERE porch_id = ? AND pkTask = ?", [
        PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchId,
        PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask.toString()
      ]);
      needRepair(false);
      var shieldsLength;
      if (shields.length == 1) {
        shieldsLength = shields[0];
      } else {
        shieldsLength = shields.last;
      }
      shieldsLength.forEach((key, value) {
        if (key == 'broken_shield' ||
            key == 'broken_gib' ||
            key == 'no_glass' ||
            key == 'replace_glass' ||
            key == 'against_tenants' ||
            key == 'no_social_info' ||
            key == 'no_top_plank' ||
            key == 'no_left_plank' ||
            key == 'no_right_plank' ||
            key == 'lift_replacing') {
          if (value == 1) {
            needRepair(true);
          }
        }
      });

      /// Конец ремонт на месте
      textLoader.value = 'Сохроняю фотографию в базу данных';
      File rotatedImage = await FlutterExifRotation.rotateImage(path: imageFile.value);
      var image = new ShieldModel(
        is_send: false,
        pkTask: ShieldStatusController.to.shieldDb[0]['pkTask'],
        porch_id: ShieldStatusController.to.shieldDb[0]['porch_id'],
        pk_address: ShieldStatusController.to.shieldDb[0]['pk_address'],
        broken_shield: ShieldStatusController.to.shieldDb[0]['broken_shield'],
        broken_gib: ShieldStatusController.to.shieldDb[0]['broken_gib'],
        no_glass: ShieldStatusController.to.shieldDb[0]['no_glass'],
        replace_glass: ShieldStatusController.to.shieldDb[0]['replace_glass'],
        against_tenants: ShieldStatusController.to.shieldDb[0]['against_tenants'],
        no_social_info: ShieldStatusController.to.shieldDb[0]['no_social_info'],
        no_top_plank: ShieldStatusController.to.shieldDb[0]['no_top_plank'],
        no_left_plank: ShieldStatusController.to.shieldDb[0]['no_left_plank'],
        no_right_plank: ShieldStatusController.to.shieldDb[0]['no_right_plank'],
        lift_replacing: ShieldStatusController.to.shieldDb[0]['lift_replacing'],
        is_repaired: isRepaired.value,
        is_display: ShieldStatusController.to.shieldDb[0]['is_display'],
        address: AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].address,
        porchNumber: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchNumber,
        img: rotatedImage.path,
        date: DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now()).toString(),
        is_broken: needRepair.value,
      );
      if (isEditPorch.value) {
        await databaseHelper.db.update("shields", image.toMap(), where: "id = ?", whereArgs: [idImage.value]);
      } else {
        await databaseHelper.db.update('shields', image.toMap(),
            where: "pkTask = ? AND porch_id = ? AND img = ''",
            whereArgs: [ShieldStatusController.to.shieldDb[0]['pkTask'].toString(), ShieldStatusController.to.shieldDb[0]['porch_id']]);
      }

      /// Поиск повторяющихся адресов
      textLoader.value = 'Смотрю есть ли двойной стенд';
      var doublePorchData = [];
      var withoutConnectionData;

      /// Получаю подъезды/экраны, которые еще не выгруженны и при этому у них совпадают адреса с текущем подъездом
      List<Map> doublePorch = await databaseHelper.db.rawQuery(
          "SELECT * FROM porchs WHERE is_change=0 AND address = ? AND porch_number = ? EXCEPT SELECT * FROM porchs WHERE pk = ? AND pkTask = ?",
          [
            AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].address.toString(),
            PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchNumber.toString(),
            PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pk,
            PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask.toString()
          ]);
      if (doublePorch.length > 0) {
        /// Получаю задачу дублирующего адреса.
        /// Если is_closed_or_not_found == 0, то продолжаю
        List<Map> taskBeforePorch =
            await databaseHelper.db.rawQuery("SELECT is_closed_or_not_found FROM tasks WHERE pk = ?", [doublePorch[0]['pkTask']]);
        if (taskBeforePorch[0]['is_closed_or_not_found'] == 0) {
          List<Map> doubleAddress =
              await databaseHelper.db.rawQuery("Select pk_task, porch_count from address where pk = ?", [doublePorch[0]['pk_address']]);
          textLoader.value = 'Формирую набор данных по двойному стенду';
          doublePorchData.add({
            'pkTask': doubleAddress[0]['pk_task'],
            'pkAddress': doublePorch[0]['pk_address'],
            'porchId': doublePorch[0]['porch_id'],
          });
        }
      }

      /// Конец поиска повторяющихся адресов

      /// Сохраняю подъезд
      final porchList = new PorchsModel(
          pk: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pk,
          isChange: true,
          pkAddress: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkAddress,
          type: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].type,
          pkTask: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask.toString(),
          address: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].address,
          porchNumber: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchNumber,
          porchId: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchId);
      await databaseHelper.db.update("porchs", porchList.toMap(), where: "porch_id = ? AND pkTask = ?", whereArgs: [
        PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchId,
        PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask.toString()
      ]);
      withoutConnectionData = {
        'porchCountAddress': porchCountAddress,
        'addressCountTasks': addressCountTasks,
        'porchCountTasks': porchCountTasks,
        'tasks': tasks,
        'address': address
      };
      await withoutConnection(withoutConnectionData, doublePorchData);
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  withoutConnection(data, doublePorchData) async {
    try {
      int porchCountAddress = data['porchCountAddress'];
      int addressCountTasks = data['addressCountTasks'];
      int porchCountTasks = data['porchCountTasks'];
      textLoader.value = 'Сохроняю фотографию';
      if (porchCountAddress > 0) {
        if (!needRepair.value) {
          porchCountAddress -= 1;
        }
      }
      textLoader.value = 'Обновляю адреса';
      final address = new AddressModel(
          pk: AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].pk,
          pkTask: int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask),
          address: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].address,
          porchCount: porchCountAddress);
      await databaseHelper.db.update("address", address.toMap(), where: "pk = ? AND pk_task = ?", whereArgs: [
        AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].pk,
        int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask)
      ]);
      if (porchCountAddress == 0) {
        textLoader.value = 'Обновляю задачу';
        final task = new TaskModel(
            pk: int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask),
            city: data['tasks'][0]['city'],
            addressCount: addressCountTasks - 1,
            porchCount: !needRepair.value ? porchCountTasks - 1 : porchCountTasks,
            type: data['tasks'][0]['type'],
            comment: data['tasks'][0]['comment'],
            date: data['tasks'][0]['date'],
            isClosedOrNotFound: data['tasks'][0]['is_closed_or_not_found'] == 1 ? true : false);
        await databaseHelper.db.update("tasks", task.toMap(),
            where: "pk = ?", whereArgs: [int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask)]);
        if (needRepair.value) {
          await isNeedRepair(porchCountAddress, addressCountTasks, porchCountTasks, data['tasks'], doublePorchData);
        } else {
          if (doublePorchData.length > 0) {
            return doublePorchAlert(doublePorchData);
          }
          if (isDoubleStand.value) {
            isDoublePorch();
          } else if (isEditPorch.value) {
            // Navigator.of(context).push(MaterialPageRoute(
            //     builder: (context) => GalleryItemWidget(pk: widget.pkTask, city: widget.city, type: widget.type, date: widget.type),
            //     fullscreenDialog: true));
          } else {
            isRepaired(false);
            needRepair(false);
            isEditPorch(false);
            isDoubleStand(false);
            await AddressController.to.getAddressList();
            Get.toNamed('/address');
          }
        }
      } else {
        final task = new TaskModel(
            pk: int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask),
            city: data['tasks'][0]['city'],
            addressCount: addressCountTasks,
            porchCount: !needRepair.value ? porchCountTasks - 1 : porchCountTasks,
            type: data['tasks'][0]['type'],
            comment: data['tasks'][0]['comment'],
            date: data['tasks'][0]['date'],
            isClosedOrNotFound: data['tasks'][0]['is_closed_or_not_found'] == 1 ? true : false);
        await databaseHelper.db.update("tasks", task.toMap(),
            where: "pk = ?", whereArgs: [int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask)]);
        if (needRepair.value) {
          await isNeedRepair(porchCountAddress, addressCountTasks, porchCountTasks, data['tasks'], doublePorchData);
        } else {
          if (doublePorchData.length > 0) {
            return doublePorchAlert(doublePorchData);
          }
          if (isDoubleStand.value) {
            isDoublePorch();
          } else if (isEditPorch.value) {
            // Navigator.of(context).push(MaterialPageRoute(
            //     builder: (context) => GalleryItemWidget(pk: widget.pkTask, city: widget.city, type: widget.type, date: widget.type),
            //     fullscreenDialog: true));
          } else {
            isRepaired(false);
            needRepair(false);
            isEditPorch(false);
            isDoubleStand(false);
            await PorchController.to.getPorchsList();
            Get.toNamed('/porchs');
          }
        }
      }
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  isNeedRepair(porchCountAddress, addressCountTasks, porchCountTasks, tasks, doublePorchData) async {
    try {
      load(true);
      Get.dialog(AlertDialog(
        title: Text('Выполнить ремонт на месте?'),
        content: Text('Состояние стенда станет "Без повреждений"'),
        actions: [
          TextButton(
            child: Text(
              "Нет",
              style: actionButtonText,
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(grey),
            ),
            onPressed: () async {
              needRepair(false);
              Get.back();
              if (porchCountAddress > 0) {
                porchCountAddress -= 1;
              }
              final address = new AddressModel(
                  pk: AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].pk,
                  pkTask: int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask),
                  address: PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].address,
                  porchCount: porchCountAddress);
              await databaseHelper.db.update("address", address.toMap(), where: "pk = ? AND pk_task = ?", whereArgs: [
                AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].pk,
                int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask)
              ]);
              if (porchCountAddress == 0) {
                final task = new TaskModel(
                    pk: int.parse(PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask),
                    city: TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].city,
                    addressCount: addressCountTasks - 1,
                    porchCount: porchCountTasks - 1,
                    type: tasks[0]['type'],
                    comment: tasks[0]['comment'],
                    date: tasks[0]['date'],
                    isClosedOrNotFound: tasks[0]['is_closed_or_not_found'] == 1 ? true : false);
                await databaseHelper.db.update("tasks", task.toMap(),
                    where: "pk = ?", whereArgs: [TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].pkTask]);
                if (doublePorchData.length > 0) {
                  return doublePorchAlert(doublePorchData);
                }
                if (isDoubleStand.value) {
                  isDoublePorch();
                } else if (isEditPorch.value) {
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => GalleryItemWidget(pk: widget.pkTask, city: widget.city, type: widget.type, date: widget.type),
                  //     fullscreenDialog: true));
                } else {
                  isRepaired(false);
                  isEditPorch(false);
                  isDoubleStand(false);
                  await AddressController.to.getAddressList();
                  Get.toNamed('/address');
                }
              } else {
                final task = new TaskModel(
                    pk: AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].pkTask,
                    city: TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].city,
                    addressCount: addressCountTasks,
                    porchCount: porchCountTasks - 1,
                    type: tasks[0]['type'],
                    comment: tasks[0]['comment'],
                    date: tasks[0]['date'],
                    isClosedOrNotFound: tasks[0]['is_closed_or_not_found'] == 1 ? true : false);
                await databaseHelper.db.update("tasks", task.toMap(),
                    where: "pk = ?", whereArgs: [AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].pkTask]);
                if (doublePorchData.length > 0) {
                  return doublePorchAlert(doublePorchData);
                }
                if (isDoubleStand.value) {
                  isDoublePorch();
                } else if (isEditPorch.value) {
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => GalleryItemWidget(
                  //         pk: widget.pkTask,
                  //         city: widget.city,
                  //         type: widget.type,
                  //         date: widget.type),
                  //     fullscreenDialog: true));
                } else {
                  isRepaired(false);
                  isEditPorch(false);
                  isDoubleStand(false);
                  await PorchController.to.getPorchsList();
                  Get.toNamed('/porchs');
                }
              }
            },
          ),
          TextButton(
            child: Text(
              "Да",
              style: actionButtonText,
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.orange),
            ),
            onPressed: () async {
              Get.back();
              needRepair(false);
              isRepaired(true);
              List<Map> shieldIsDisplay =
                  await databaseHelper.db.rawQuery("SELECT is_display FROM shields WHERE porch_id = ? AND pkTask = ?", [
                PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchId,
                PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask.toString()
              ]);
              List shields = [
                {
                  'pkTask': PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask.toString(),
                  'is_send': false,
                  'pk_address': PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkAddress,
                  'porch_id': PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchId,
                  'broken_shield': false,
                  'broken_gib': false,
                  'no_glass': false,
                  'replace_glass': false,
                  'against_tenants': false,
                  'no_social_info': false,
                  'no_top_plank': false,
                  'no_left_plank': false,
                  'no_right_plank': false,
                  'lift_replacing': false,
                  'is_repaired': isRepaired.value,
                  'is_display': shieldIsDisplay[0]['is_display'] == 1 ? true : false,
                }
              ];
              ShieldStatusController.to.shieldDb.clear();
              ShieldStatusController.to.shieldDb.add(shields[0]);
              ShieldStatusController.to.shieldDb.refresh();
              shields.map((job) => ShieldModel.fromJson(job)).toList().forEach((shield) async {
                await databaseHelper.db.insert("shields", shield.toMap());
              });
              CameraScreenState().onImageButtonPressed(ImageSource.camera);
            },
          ),
        ],
      ));
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  doublePorchAlert(data) async {
    try {
      load(true);
      Get.dialog(AlertDialog(
        title: Text('Уведомление'),
        content: Text('В этом подъезде есть еще один стенд'),
        actions: [
          TextButton(
            child: Text(
              "Нет",
              style: actionButtonText,
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.grey),
            ),
            onPressed: () {
              Get.back();
            },
          ),
          TextButton(
            child: Text(
              "Перейти",
              style: actionButtonText,
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.orange),
            ),
            onPressed: () async {
              Get.back();
              isDoubleStand(true);
              needRepair(false);
              isEditPorch(false);

              /// Получаю индекс элемента задачи в списке и присваиваю его переменной в AddressController
              AddressController.to.taskIndex.value = TaskController.to.data.indexWhere((i) => i.pk == data[0]['pkTask']).toString();
              await AddressController.to.getAddressList();

              /// Получаю индекс элемента адреса в списке и присваиваю его переменной в PorchController
              PorchController.to.addressIndex.value = AddressController.to.data.indexWhere((i) => i.pk == data[0]['pkAddress']).toString();
              await PorchController.to.getPorchsList();

              /// Получаю индекс элемента подъезда в списке и присваиваю его переменной в ShieldStatusController
              ShieldStatusController.to.porchIndex.value =
                  PorchController.to.data.indexWhere((i) => i.porchId == data[0]['porchId']).toString();
              await ShieldStatusController.to.getShieldStatusList();
              Get.toNamed('/shield_status');
            },
          ),
        ],
      ));
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    } finally {
      load(false);
    }
  }

  isDoublePorch() async {
    try {
      List<Map> beforePorch = await databaseHelper.db
          .rawQuery("SELECT * FROM porchs WHERE address = ? AND porch_number = ? EXCEPT SELECT * FROM porchs WHERE pk = ? AND pkTask = ?", [
        AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].address.toString(),
        PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchNumber.toString(),
        PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pk,
        PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].pkTask.toString()
      ]);
      List<Map> beforeAddress = await databaseHelper.db.rawQuery("Select * from address where pk = ? and pk_task = ?", [
        beforePorch[0]['pk_address'],
        int.tryParse(beforePorch[0]['pkTask']),
      ]);
      if (beforeAddress[0]['porch_count'] == 0) {
        /// Получаю индекс элемента задачи в списке и присваиваю его переменной в AddressController
        AddressController.to.taskIndex.value = TaskController.to.data.indexWhere((i) => i.pk == beforeAddress[0]['pk_task']).toString();
        await AddressController.to.getAddressList();
        Get.toNamed('/address');
        isDoubleStand(false);
        isEditPorch(false);
      } else {
        /// Получаю индекс элемента задачи в списке и присваиваю его переменной в AddressController
        AddressController.to.taskIndex.value = TaskController.to.data.indexWhere((i) => i.pk == beforeAddress[0]['pk_task']).toString();
        await AddressController.to.getAddressList();

        /// Получаю индекс элемента адреса в списке и присваиваю его переменной в PorchController
        PorchController.to.addressIndex.value = AddressController.to.data.indexWhere((i) => i.pk == beforeAddress[0]['pk']).toString();
        await PorchController.to.getPorchsList();
        Get.toNamed('/porchs');
        isDoubleStand(false);
        isEditPorch(false);
      }
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
