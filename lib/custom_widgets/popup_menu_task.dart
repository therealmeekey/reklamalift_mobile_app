import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:reklamalift/controller/gallery_controller.dart';
import 'package:reklamalift/controller/login_controller.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

Widget childPopupTask(BuildContext context) => IconButton(
    icon: Icon(Icons.more_vert, color: white),
    onPressed: () {
      final action = CupertinoActionSheet(
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: Text("Галлерея", style: cupertinoMenuText,),
            isDefaultAction: true,
            onPressed: () {
              Get.back();
              Get.toNamed('/gallery');
              Get.lazyPut(() => GalleryController());
              GalleryController.to.getImageList();
            },
          ),
          CupertinoActionSheetAction(
            child: Text("Обновить данные", style: cupertinoMenuText,),
            isDefaultAction: true,
            onPressed: () {
              Get.back();
              TaskController.to.updateData();
            },
          ),
          CupertinoActionSheetAction(
            child: Text("Очистить память", style: cupertinoMenuText,),
            isDefaultAction: true,
            onPressed: () {
              Get.back();
              Get.dialog(
                AlertDialog(
                  title: Text(
                    'Убедитесь, что все данные выгружены. Введите пароль для подтверждения действия',
                    style: errorInfoText,
                  ),
                  content: TextField(
                    controller: TaskController.to.passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        labelText: 'Пароль',
                        labelStyle: TextStyle(fontFamily: "SanFrancisco", color: grey),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: orange, width: 1.0),
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        errorText: TaskController.to.errorText.value),
                  ),
                  actions: [
                    TextButton(
                      child: Text(
                        "Да",
                        style: actionButtonText,
                      ),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(red),
                      ),
                      onPressed: () async {
                        await TaskController.to.clearRAM();
                      },
                    ),
                    TextButton(
                      child: Text(
                        "Нет",
                        style: actionButtonText,
                      ),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(grey),
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    ),
                  ],
                ),
              );
            },
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text("Выйти из аккаунта", style: cupertinoMenuCancelText),
          onPressed: () {
            Get.dialog(
              AlertDialog(
                title: Text('Вы уверены?'),
                content: Text('Вы уверены, что хотите выйти из приложения?'),
                actions: [
                  TextButton(
                      child: Text(
                        "Выйти",
                        style: actionButtonText,
                      ),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(red),
                      ),
                      onPressed: () {
                        Get.back();
                        Get.lazyPut(() => LoginController());
                        LoginController.to.logout();
                      }),
                  TextButton(
                    child: Text(
                      "Нет",
                      style: actionButtonText,
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(grey),
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  )
                ],
              ),
            );
          },
        ),
      );
      showCupertinoModalPopup(context: context, builder: (context) => action);
    });
