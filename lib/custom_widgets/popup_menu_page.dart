import 'package:flutter/material.dart';
import 'package:reklamalift/controller/gallery_controller.dart';
import 'package:get/get.dart';
import 'package:reklamalift/utility/colors.dart';

Widget childPopupPage() => IconButton(
    icon: Icon(Icons.image_outlined, color: white),
    onPressed: () async {
      Get.lazyPut(() => GalleryController());
      await GalleryController.to.getImageList();
      Get.toNamed('/gallery');
    });
