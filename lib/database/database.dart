import 'package:path/path.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;
  static Database _db;
  Map<int, String> migrationScripts = {
    1: 'ALTER TABLE porchs ADD COLUMN pkTask TEXT;',
    2: 'ALTER TABLE images ADD COLUMN pk_address INTEGER;',
    3: 'ALTER TABLE images ADD COLUMN is_broken BOOLEAN CHECK (is_broken IN (0,1));',
    4: 'ALTER TABLE images ADD COLUMN img TEXT;',
    5: 'ALTER TABLE shields ADD COLUMN is_repaired BOOLEAN CHECK (is_repaired IN (0,1));',
    6: 'ALTER TABLE shields ADD COLUMN is_display BOOLEAN CHECK (is_display IN (0,1));',
    7: 'ALTER TABLE porchs ADD COLUMN type TEXT;',
    8: 'DROP TABLE images;',
    9: 'ALTER TABLE shields ADD COLUMN address TEXT;',
    10: 'ALTER TABLE shields ADD COLUMN porchNumber TEXT;',
    11: 'ALTER TABLE shields ADD COLUMN img TEXT;',
    12: 'ALTER TABLE shields ADD COLUMN date TEXT;',
    13: 'ALTER TABLE shields ADD COLUMN is_broken BOOLEAN CHECK (is_broken IN (0,1));',
  };

  DatabaseHelper.internal();

  Future initDatabase() async {
    try {
      int nbrMigrationScripts = migrationScripts.length;
      _db = await openDatabase(
        join(await getDatabasesPath(), "reklamalift.db"),
        version: nbrMigrationScripts,
        onCreate: _onCreate,
        onUpgrade: (db, oldVersion, newVersion) async {
          for (int i = oldVersion + 1; i <= newVersion; i++) {
            await db.execute(migrationScripts[i]);
          }
        },
      );
    } catch (exception, stacktrace) {
      print(exception);
      print(stacktrace);
      await Sentry.captureException(
        exception,
        stackTrace: stacktrace,
      );
    }
    return db;
  }

  Database get db {
    return _db;
  }

  void _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE tasks(id INTEGER PRIMARY KEY AUTOINCREMENT, "
        "pk INTEGER UNIQUE, "
        "city TEXT, "
        "address_count INTEGER, "
        "porch_count INTEGER, "
        "type TEXT, "
        "comment TEXT, "
        "date TEXT, "
        "is_closed_or_not_found BOOLEAN CHECK (is_closed_or_not_found IN (0,1)));");
    await db.execute("CREATE TABLE address(id INTEGER PRIMARY KEY AUTOINCREMENT, "
        "pk INTEGER, "
        "pk_task INTEGER, "
        "address TEXT, "
        "porch_count INTEGER);");
    await db.execute(
      "CREATE TABLE porchs(id INTEGER PRIMARY KEY AUTOINCREMENT, "
      "is_change BOOLEAN CHECK (is_change IN (0,1)),  "
      "pk INTEGER, "
      "type TEXT, "
      "pk_address INTEGER, "
      "pkTask TEXT, "
      "address TEXT, "
      "porch_number TEXT, "
      "porch_id INTEGER);",
    );
    await db.execute("CREATE TABLE shields(id INTEGER PRIMARY KEY AUTOINCREMENT, "
        "is_send BOOLEAN CHECK (is_send IN (0,1)) DEFAULT 0, "
        "pk_address INTEGER, "
        "porch_id INTEGER, "
        "pkTask TEXT, "
        "broken_shield BOOLEAN CHECK (broken_shield IN (0,1)), "
        "broken_gib BOOLEAN CHECK (broken_gib IN (0,1)), "
        "no_glass BOOLEAN CHECK (no_glass IN (0,1)), "
        "replace_glass BOOLEAN CHECK (replace_glass IN (0,1)), "
        "against_tenants BOOLEAN CHECK (against_tenants IN (0,1)), "
        "no_social_info BOOLEAN CHECK (no_social_info IN (0,1)), "
        "no_top_plank BOOLEAN CHECK (no_top_plank IN (0,1)), "
        "no_left_plank BOOLEAN CHECK (no_left_plank IN (0,1)), "
        "no_right_plank BOOLEAN CHECK (no_right_plank IN (0,1)), "
        "lift_replacing BOOLEAN CHECK (lift_replacing IN (0,1)),"
        "is_repaired BOOLEAN CHECK (is_repaired IN (0,1)),"
        "is_display BOOLEAN CHECK (is_display IN (0,1)),"
        "address TEXT,"
        "porchNumber TEXT,"
        "img TEXT,"
        "date TEXT,"
        "is_broken BOOLEAN CHECK (is_broken IN (0,1)));");
  }
}
