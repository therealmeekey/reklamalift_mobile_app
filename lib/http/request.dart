import 'package:http/http.dart' as http;
import 'package:reklamalift/http/url.dart';

class Request {
  final String url;
  final dynamic body;
  final dynamic headers;

  Request({this.url, this.body, this.headers});

  Future<http.Response> post() {
    return http.post(Uri.parse(API_URL + url), body: body, headers: headers);
  }

  Future<http.Response> get() {
    return http.get(Uri.parse(API_URL + url), headers: headers);
  }

  Future<http.Response> delete() {
    return http.delete(Uri.parse(API_URL + url), headers: headers);
  }
}
