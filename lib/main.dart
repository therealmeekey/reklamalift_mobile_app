import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:reklamalift/utility/router.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import 'database/database.dart';
import 'database/injection.dart';

const bool isProduction = bool.fromEnvironment('dart.vm.product');
DatabaseHelper databaseHelper = Injection.injector.get();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Injection.initInjection();
  await GetStorage.init();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  var initialRoute;
  if (GetStorage().read('token') == null) {
    initialRoute = '/login';
  } else {
    initialRoute = '/splash';
  }
  await SentryFlutter.init(
    (options) => options.dsn = 'https://@o505532.ingest.sentry.io/5593989',
    appRunner: () => runApp(
      GetMaterialApp(
        title: 'reklamalift',
        debugShowCheckedModeBanner: false,
        defaultTransition: Transition.cupertino,
        getPages: Routers.route,
        initialRoute: initialRoute,
      ),
    ),
  );
}
