import 'dart:ui';

import 'package:flutter/material.dart';

const Color background = Color.fromARGB(1, 189, 195, 199);
const Color black = Color.fromARGB(255, 0, 0, 0);
const Color white = Color.fromARGB(255, 255, 255, 255);
const Color red = Color.fromARGB(255, 255, 0, 0);
const Color grey = Color.fromRGBO(109, 108, 113, 1);
const Color orange = Color.fromRGBO(239, 147, 36, 1);
const Color green = Colors.green;
const Color blue = Colors.blue;
