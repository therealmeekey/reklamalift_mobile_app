import 'package:get/get.dart';
import 'package:reklamalift/view/address_view.dart';
import 'package:reklamalift/view/camera_view.dart';
import 'package:reklamalift/view/gallery_item_view.dart';
import 'package:reklamalift/view/gallery_view.dart';
import 'package:reklamalift/view/login_view.dart';
import 'package:reklamalift/view/maps_view.dart';
import 'package:reklamalift/view/porch_view.dart';
import 'package:reklamalift/view/shield_status_view.dart';
import 'package:reklamalift/view/splash_view.dart';
import 'package:reklamalift/view/task_view.dart';

class Routers {
  static final route = [
    GetPage(
      name: '/login',
      page: () => LoginView(),
    ),
    GetPage(
      name: '/splash',
      page: () => SplashView(),
    ),
    GetPage(
      name: '/task',
      page: () => TaskView(),
    ),
    GetPage(
      name: '/address',
      page: () => AddressView(),
    ),
    GetPage(
      name: '/porchs',
      page: () => PorchView(),
    ),
    GetPage(
      name: '/shield_status',
      page: () => ShieldStatusView(),
    ),
    GetPage(
      name: '/camera',
      page: () => CameraView(),
    ),
    GetPage(
      name: '/gallery',
      page: () => GalleryView(),
    ),
    GetPage(
      name: '/gallery_item',
      page: () => GalleryItemView(),
    ),
    GetPage(
      name: '/maps',
      page: () => MapsView(),
    ),
  ];
}
