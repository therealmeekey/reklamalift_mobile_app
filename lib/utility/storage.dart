import 'package:get_storage/get_storage.dart';
import 'package:reklamalift/models/LoginModel.dart';

saveLoginDate(Map responseJson) async {
  if ((responseJson != null && !responseJson.isEmpty)) {
    await GetStorage().write('token', LoginModel.fromJson(responseJson).token);
    await GetStorage().write('email', LoginModel.fromJson(responseJson).email);
  } else {
    print("error write to disk");
  }
}
