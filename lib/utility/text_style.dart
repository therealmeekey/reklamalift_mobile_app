import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'colors.dart';

const appBarText = TextStyle(
  color: white,
  fontFamily: "SanFrancisco",
  fontWeight: FontWeight.w400,
  fontSize: 18,
  letterSpacing: -0.42857,
);

const typeTaskText = TextStyle(
  color: red,
  fontFamily: "SanFrancisco",
  fontWeight: FontWeight.w700,
  fontSize: 18,
);
const typeTaskGalleryText = TextStyle(
  color: red,
  fontFamily: "SanFrancisco",
  fontWeight: FontWeight.w700,
  fontSize: 15,
);

const cityNameText = TextStyle(
  color: black,
  fontFamily: "SanFrancisco",
  fontWeight: FontWeight.w400,
  fontSize: 14,
);

const buttonText = TextStyle(
  color: white,
  fontFamily: "SanFrancisco",
  fontWeight: FontWeight.w400,
  fontSize: 16,
);

const errorInfoText = TextStyle(
  fontFamily: "SanFrancisco",
  fontSize: 15,
  color: black,
  fontWeight: FontWeight.w400,
);

const listTitleActiveText = TextStyle(
  fontSize: 20,
  fontFamily: "SanFrancisco",
  color: green,
  fontWeight: FontWeight.w900,
);

const listTitleInActiveText = TextStyle(
  fontSize: 20,
  fontFamily: "SanFrancisco",
  color: grey,
  fontWeight: FontWeight.w900,
);

const nameShieldStatusText = TextStyle(
  fontSize: 18,
  fontFamily: "SanFrancisco",
  color: white,
  fontWeight: FontWeight.w500,
);

const descriptionText = TextStyle(
  fontFamily: "SanFrancisco",
  fontSize: 12,
  color: grey,
);

const textLoader = TextStyle(
  color: grey,
  fontFamily: "SanFrancisco",
  fontSize: 14,
);

const actionButtonText = TextStyle(color: white, fontSize: 14);

const cupertinoMenuText = TextStyle(
  color: blue,
  fontSize: 18,
  fontFamily: "SanFrancisco",
  fontWeight: FontWeight.w400,
);

const cupertinoMenuCancelText = TextStyle(
  color: red,
  fontSize: 18,
  fontFamily: "SanFrancisco",
  fontWeight: FontWeight.w400,
);
