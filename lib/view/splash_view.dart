import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reklamalift/controller/login_controller.dart';
import 'package:reklamalift/controller/splash_controller.dart';
import 'package:get/get.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

class SplashView extends StatelessWidget {
  final controller = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: Get.width,
                  height: Get.height * 0.5,
                  margin: EdgeInsets.only(top: Get.height * 0.129),
                  child: Image.asset(
                    "assets/images/logo2.png",
                  ),
                ),
              ),
              Center(
                child: SizedBox(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CupertinoActivityIndicator(
                        radius: 20,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: Get.height * 0.00615),
                          child: Obx(
                            () => Text(
                              controller.textLoader.value,
                              textAlign: TextAlign.center,
                              style: textLoader,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Get.height * 0.0184,
                      ),
                      Container(
                        width: Get.width * 0.2,
                        child: TextButton(
                          onPressed: () {
                            Get.lazyPut(() => LoginController());
                            Get.find<LoginController>().logout();
                          },
                          child: Text('Выход', style: TextStyle(color: blue)),
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(white),
                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(borderRadius: BorderRadius.circular(50), side: BorderSide(color: blue)))),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
