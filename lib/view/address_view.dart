import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reklamalift/controller/address_controller.dart';
import 'package:get/get.dart';
import 'package:reklamalift/controller/porch_controller.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:reklamalift/custom_widgets/popup_menu_page.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

// ignore: must_be_immutable
class AddressView extends StatelessWidget {
  final controller = Get.put(AddressController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (controller.load.value) {
          return Container(
            child: Center(
              child: CupertinoActivityIndicator(
                radius: 20,
              ),
            ),
            color: white,
          );
        } else {
          return WillPopScope(
            onWillPop: () async => false,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: orange,
                title: const Text(
                  "Адреса",
                  textAlign: TextAlign.center,
                  style: appBarText,
                ),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () => Get.offAndToNamed('/task'),
                ),
                actions: <Widget>[
                  childPopupPage(),
                ],
                centerTitle: true,
              ),
              body: Container(
                decoration: BoxDecoration(color: background),
                child: Stack(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: Get.width * 0.0533, top: Get.height * 0.01477),
                            child: Text(
                              TaskController.to.data[int.parse(controller.taskIndex.value)].type,
                              textAlign: TextAlign.left,
                              style: typeTaskText,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: Get.width * 0.0533, top: Get.height * 0.00615),
                            child: Text(
                              TaskController.to.data[int.parse(controller.taskIndex.value)].city,
                              textAlign: TextAlign.left,
                              style: cityNameText,
                            ),
                          ),
                        ),
                        Divider(
                          color: black,
                          height: Get.height * 0.00615,
                          thickness: 0,
                          indent: 0,
                          endIndent: 0,
                        ),
                        Container(
                          height: Get.height - MediaQuery.of(context).padding.top - kToolbarHeight - Get.height * 0.0751,
                          width: Get.width,
                          margin: EdgeInsets.only(left: Get.width * 0.0533, right: Get.width * 0.0533),
                          child: Stack(
                            children: [
                              ListView.builder(
                                padding: EdgeInsets.only(bottom: Get.height * 0.125),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: controller.data.length,
                                itemBuilder: (context, index) {
                                  return _addressCard(index);
                                },
                              ),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  margin: EdgeInsets.only(bottom: Get.height * 0.0492),
                                  width: Get.width,
                                  height: Get.height * 0.0714,
                                  child: TextButton(
                                    onPressed: () => Get.toNamed('/maps'),
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all<Color>(grey),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(30),
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      "На карте",
                                      textAlign: TextAlign.center,
                                      style: buttonText,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget _addressCard(index) {
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      child: Card(
        semanticContainer: true,
        child: InkWell(
          onTap: () {
            Get.toNamed('/porchs');
            Get.lazyPut(() => PorchController());
            PorchController.to.addressIndex.value = index.toString();
            PorchController.to.getPorchsList();
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                leading: Text("${index + 1}", textAlign: TextAlign.right, style: listTitleInActiveText),
                title: Text(
                  controller.data[index].address,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 20,
                    fontFamily: "SanFrancisco",
                  ),
                ),
                subtitle: Text(
                  "Подъездов: ${controller.data[index].porchCount}",
                  style: TextStyle(
                    fontFamily: "SanFrancisco",
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
