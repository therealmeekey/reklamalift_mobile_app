import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:reklamalift/controller/gallery_controller.dart';
import 'package:reklamalift/controller/gallery_item_controller.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

class GalleryView extends StatelessWidget {
  final controller = Get.put(GalleryController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: orange,
          title: const Text(
            "Фотографии",
            textAlign: TextAlign.center,
            style: appBarText,
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Get.back(),
          ),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.cloud_upload_outlined),
              onPressed: () {
                controller.sendData();
              },
            ),
          ],
        ),
        body: Obx(
          () => controller.load.value
              ? Center(
                  child: SizedBox(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CupertinoActivityIndicator(
                          radius: 30,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            margin: EdgeInsets.only(top: Get.height * 0.00615),
                            child: Text(
                              controller.countImage.value.toString() +
                                  "/" +
                                  controller.fullImage.value.toString() +
                                  " шт." +
                                  "\n${controller.step.value}\nВыгружаю фотографии",
                              textAlign: TextAlign.center,
                              style: textLoader,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : Container(
                  decoration: BoxDecoration(color: background),
                  margin: EdgeInsets.only(left: Get.width * 0.053, top: Get.height * 0.01477, right: Get.width * 0.053),
                  child: controller.data.length > 0
                      ? ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: controller.data.length,
                          itemBuilder: (context, index) {
                            return galleryCard(index);
                          },
                        )
                      : Center(
                          child: Container(
                            child: Text('Фотографий не обнаружено.', style: errorInfoText),
                          ),
                        ),
                ),
        ),
      ),
    );
  }

  Widget galleryCard(index) {
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      child: Card(
        semanticContainer: true,
        child: InkWell(
          onTap: () {
            Get.toNamed('/gallery_item');
            Get.lazyPut(() => GalleryItemController());
            GalleryItemController.to.taskIndex.value = index;
            GalleryItemController.to.getPhotos();
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                leading: Row(crossAxisAlignment: CrossAxisAlignment.stretch, mainAxisSize: MainAxisSize.min, children: [
                  Text(
                    controller.data[index].imageSend.toString(),
                    textAlign: TextAlign.right,
                    style: listTitleActiveText,
                  ),
                  Text(
                    '/' + controller.data[index].countImage.toString(),
                    textAlign: TextAlign.right,
                    style: listTitleInActiveText,
                  ),
                ]),
                title: Text(
                  "${controller.data[index].type}",
                  textAlign: TextAlign.left,
                  style: typeTaskGalleryText,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "\n${controller.data[index].city}",
                      textAlign: TextAlign.left,
                      style: cityNameText,
                    ),
                    SizedBox(
                      height: Get.height * 0.00492,
                    ),
                    Text(
                      "${controller.data[index].date}",
                      textAlign: TextAlign.left,
                      style: descriptionText,
                    ),
                    SizedBox(
                      height: Get.height * 0.00492,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
