import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reklamalift/controller/address_controller.dart';
import 'package:reklamalift/controller/porch_controller.dart';
import 'package:reklamalift/controller/shield_status_controller.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:get/get.dart';
import 'package:reklamalift/custom_widgets/popup_menu_page.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

// ignore: must_be_immutable
class ShieldStatusView extends StatelessWidget {
  final controller = Get.put(ShieldStatusController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (controller.load.value) {
          return Container(
            child: Center(
                child: CupertinoActivityIndicator(
              radius: 20,
            )),
            color: white,
          );
        } else {
          return WillPopScope(
            onWillPop: () async => false,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: orange,
                title: const Text(
                  "Состояние стенда",
                  textAlign: TextAlign.center,
                  style: appBarText,
                ),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () => Get.offAndToNamed('/porchs'),
                ),
                actions: <Widget>[
                  childPopupPage(),
                ],
                centerTitle: true,
              ),
              body: Container(
                decoration: BoxDecoration(color: background),
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: Get.width * 0.053, top: Get.height * 0.01477),
                            child: Text(
                              Get.find<TaskController>().data[int.parse(AddressController.to.taskIndex.value)].type,
                              textAlign: TextAlign.left,
                              style: typeTaskText,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: Get.width * 0.053, top: Get.height * 0.00615),
                            child: Text(
                              Get.find<TaskController>().data[int.parse(AddressController.to.taskIndex.value)].city,
                              textAlign: TextAlign.left,
                              style: cityNameText,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: Get.width * 0.053, top: Get.height * 0.00615),
                            child: Text(
                              AddressController.to.data[int.parse(PorchController.to.addressIndex.value)].address +
                                  ', ${PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].type} ' +
                                  PorchController.to.data[int.parse(ShieldStatusController.to.porchIndex.value)].porchNumber.toString(),
                              textAlign: TextAlign.left,
                              style: cityNameText,
                            ),
                          ),
                        ),
                        Divider(
                          color: black,
                          height: Get.height * 0.00615,
                          thickness: 0,
                          indent: 0,
                          endIndent: 0,
                        ),
                        Container(
                          height: Get.height - MediaQuery.of(context).padding.top - kToolbarHeight - Get.height * 0.109,
                          width: Get.width,
                          margin: EdgeInsets.only(left: Get.width * 0.053, right: Get.width * 0.053),
                          child: Stack(
                            children: [
                              ListView.builder(
                                padding: EdgeInsets.only(bottom: Get.height * 0.125),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: controller.data.length,
                                itemBuilder: (context, index) {
                                  return _shieldCard(index);
                                },
                              ),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  margin: EdgeInsets.only(bottom: Get.height * 0.0492),
                                  width: Get.width,
                                  height: Get.height * 0.0714,
                                  child: TextButton(
                                    onPressed: () => {controller.saveData()},
                                    style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty.all<Color>(grey),
                                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
                                    child: Text(
                                      "Выполнить осмотр",
                                      textAlign: TextAlign.center,
                                      style: buttonText,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget _shieldCard(index) {
    return Container(
        width: Get.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        child: Card(
            color: controller.getColor(controller.data[index]['value']),
            semanticContainer: true,
            child: InkWell(
                onTap: () {
                  controller.changeValue(controller.data[index]['name'], controller.data[index]['value']);
                },
                child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, mainAxisSize: MainAxisSize.min, children: [
                  ListTile(
                    title: Text(
                      controller.data[index]['name'].toString(),
                      textAlign: TextAlign.center,
                      style: nameShieldStatusText,
                    ),
                  ),
                ]))));
  }
}
