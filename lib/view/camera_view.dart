import 'dart:async';
import 'dart:io';
import 'package:get/get.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:reklamalift/controller/camera_controller.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

// ignore: must_be_immutable
class CameraView extends StatefulWidget {
  @override
  CameraScreenState createState() => CameraScreenState();
}

class CameraScreenState extends State<CameraView> {
  final controller = Get.put(CameraController());
  PickedFile _imageFile;
  dynamic _pickImageError;
  String _retrieveDataError;

  final ImagePicker _picker = ImagePicker();

  void onImageButtonPressed(ImageSource source) async {
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.camera, imageQuality: 60);
      if (mounted) {
        setState(() {
          _imageFile = pickedFile;
        });
      }
      controller.imageFile.value = pickedFile.path;
      controller.upload();
    } catch (e) {
      if (mounted) {
        setState(() {
          _pickImageError = e;
        });
      }
    }
  }

  void initState() {
    onImageButtonPressed(ImageSource.camera);
    super.initState();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      return Obx(() => Image.file(File(controller.imageFile.value), fit: BoxFit.contain));
    } else if (_pickImageError != null) {
      return Text(
        'Ошибка: $_pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return const Center(
          child: Text(
        'Вы еще не сделали фото',
        textAlign: TextAlign.center,
      ));
    }
  }

  Future<void> retrieveLostData() async {
    final LostData response = await _picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      controller.imageFile.value = response.file.path;
      if (mounted) {
        setState(() {
          _imageFile = response.file;
        });
      }
    } else {
      _retrieveDataError = response.exception.code;
    }
  }

  Text _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final Text result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: orange,
          title: const Text(
            "Фотография",
            textAlign: TextAlign.center,
            style: appBarText,
          ),
          centerTitle: true,
          leading: Obx(
            () => controller.needRepair.value
                ? Container()
                : IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () => Get.offAndToNamed('/shield_status'),
                  ),
          ),
        ),
        body: Center(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: _previewImage(),
                ),
                Flexible(
                  flex: 2,
                  child: Obx(
                    () => Center(
                      child: !controller.load.value
                          ? ButtonBar(
                              alignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                TextButton(
                                  onPressed: () => onImageButtonPressed(ImageSource.camera),
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all<Color>(grey),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    child: Text(
                                      'Повторить',
                                      style: appBarText,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          : Center(
                              child: SizedBox(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    CupertinoActivityIndicator(
                                      radius: 30,
                                    ),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                        margin: EdgeInsets.only(top: Get.height * 0.00615),
                                        child: Text(
                                          controller.textLoader.value,
                                          textAlign: TextAlign.center,
                                          style: textLoader,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
