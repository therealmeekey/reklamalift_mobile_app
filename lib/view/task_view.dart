import 'package:get/get.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reklamalift/controller/address_controller.dart';
import 'package:reklamalift/controller/login_controller.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:reklamalift/custom_widgets/popup_menu_task.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

class TaskView extends StatelessWidget {
  final controller = Get.put(TaskController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (controller.load.value) {
          return Container(
            child: Center(
              child: CupertinoActivityIndicator(
                radius: 20,
              ),
            ),
            color: white,
          );
        } else {
          return WillPopScope(
            onWillPop: () async => false,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: orange,
                leading: Container(),
                title: const Text(
                  "Задачи",
                  textAlign: TextAlign.center,
                  style: appBarText,
                ),
                centerTitle: true,
                actions: <Widget>[
                  childPopupTask(context),
                ],
              ),
              body: Container(
                decoration: BoxDecoration(color: background),
                margin: EdgeInsets.only(left: Get.width * 0.0533, top: Get.height * 0.01477, right: Get.width * 0.0533),
                child: controller.data.length > 0
                    ? ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: controller.data.length,
                        itemBuilder: (context, index) {
                          return taskCard(index);
                        },
                      )
                    : Center(
                        child: Container(
                          child: Text(
                            'Задачи не найдены.',
                            style: errorInfoText,
                          ),
                        ),
                      ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget taskCard(index) {
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      child: Card(
        semanticContainer: true,
        child: InkWell(
          onTap: () {
            Get.toNamed('/address');
            Get.lazyPut(() => AddressController());
            AddressController.to.taskIndex.value = index.toString();
            AddressController.to.getAddressList();
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                leading: Icon(Icons.work, color: Colors.grey),
                title: Text(
                  controller.data[index].type.toString(),
                  textAlign: TextAlign.left,
                  style: typeTaskGalleryText,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      '\n${controller.data[index].city.toString()}',
                      textAlign: TextAlign.left,
                      style: errorInfoText,
                    ),
                    Text("Адресов: ${controller.data[index].addressCount}\nПодъездов: ${controller.data[index].porchCount}",
                        textAlign: TextAlign.left, style: descriptionText),
                    Text(controller.data[index].date.toString(), textAlign: TextAlign.left, style: descriptionText),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  controller.data[index].comment.toString() == '' ? 'Комментариев нет' : controller.data[index].comment.toString(),
                  style: TextStyle(
                    color: black.withOpacity(0.6),
                    fontFamily: "SanFrancisco",
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
