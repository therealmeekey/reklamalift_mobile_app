import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:reklamalift/controller/login_controller.dart';
import 'package:get/get.dart';
import 'package:flutter/services.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final controller = Get.put(LoginController());

  FocusNode emailFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();
  bool focusState = false;

  @override
  void dispose() {
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    super.dispose();
  }

  void _emailFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(emailFocusNode);
    });
  }

  void _passwordFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(passwordFocusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(0.01089, 1.09827),
              end: Alignment(0.98911, -0.09827),
              stops: [
                0,
                1,
              ],
              colors: [
                orange,
                grey,
              ],
            ),
          ),
          height: Get.height,
          width: Get.width,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: Get.width,
                    height: Get.height * 0.3,
                    child: Image.asset('assets/images/logo2.png'),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                    ),
                    margin: EdgeInsets.only(left: Get.width * 0.04, right: Get.width * 0.04),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: Get.height * 0.0369,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: Get.width * 0.0266, right: Get.width * 0.0266),
                          child: TextField(
                            focusNode: emailFocusNode,
                            keyboardType: TextInputType.emailAddress,
                            onTap: _emailFocus,
                            controller: controller.emailTextController,
                            decoration: InputDecoration(
                              labelText: 'Email',
                              labelStyle: TextStyle(fontFamily: "SanFrancisco", color: emailFocusNode.hasFocus ? orange : grey),
                              suffixIcon: Icon(
                                Icons.email,
                                color: emailFocusNode.hasFocus ? orange : grey,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: orange, width: 1.0),
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Get.height * 0.0246,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: Get.width * 0.0266, right: Get.width * 0.0266),
                          child: TextField(
                            textInputAction: TextInputAction.go,
                            onSubmitted: (value) {
                              if (controller.emailTextController.text != '' && controller.passwordController.text != '') {
                                controller.loginRequest();
                              }
                            },
                            focusNode: passwordFocusNode,
                            onTap: _passwordFocus,
                            controller: controller.passwordController,
                            obscureText: true,
                            decoration: InputDecoration(
                              labelText: 'Пароль',
                              labelStyle: TextStyle(fontFamily: "SanFrancisco", color: passwordFocusNode.hasFocus ? orange : grey),
                              suffixIcon: Icon(
                                Icons.visibility_off,
                                color: passwordFocusNode.hasFocus ? orange : grey,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: orange, width: 1.0),
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Get.height * 0.0369,
                        ),
                        Obx(
                          () => Container(
                            margin: EdgeInsets.only(left: Get.width * 0.173, bottom: Get.height * 0.049, right: Get.width * 0.173),
                            width: Get.width,
                            height: Get.height * 0.066,
                            child: controller.load.value
                                ? Center(
                                    child: CupertinoActivityIndicator(
                                      radius: 20,
                                    ),
                                  )
                                : RaisedButton(
                                    onPressed: () => {
                                      FocusScope.of(context).requestFocus(FocusNode()),
                                      if (controller.emailTextController.text != '' && controller.passwordController.text != '')
                                        {controller.loginRequest()}
                                    },
                                    color: grey,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(30)),
                                    ),
                                    textColor: white,
                                    padding: EdgeInsets.all(0),
                                    child: Text(
                                      "Войти",
                                      textAlign: TextAlign.center,
                                      style: buttonText,
                                    ),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
