import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reklamalift/controller/gallery_controller.dart';
import 'package:reklamalift/controller/gallery_item_controller.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:get/get.dart';
import 'package:reklamalift/utility/text_style.dart';

// ignore: must_be_immutable
class GalleryItemView extends StatefulWidget {
  @override
  _GalleryItemViewState createState() => _GalleryItemViewState();
}

class _GalleryItemViewState extends State<GalleryItemView> {
  final controller = Get.put(GalleryItemController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: orange,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Get.back(),
        ),
        title: const Text(
          "Фотографии",
          textAlign: TextAlign.center,
          style: appBarText,
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.delete_forever),
            onPressed: () {
              Get.dialog(
                AlertDialog(
                  title: Text('Вы уверены?'),
                  content: Text('Перед удалением убедитесь, что все фотографии были отправлены и получены'),
                  actions: [
                    TextButton(
                      child: Text(
                        "Удалить",
                        style: actionButtonText,
                      ),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(red),
                      ),
                      onPressed: () {
                        Get.back();
                        controller.removeData();
                      },
                    ),
                    TextButton(
                      child: Text(
                        "Закрыть",
                        style: actionButtonText,
                      ),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(grey),
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    ),
                  ],
                ),
              );
            },
          )
        ],
      ),
      body: Obx(
        () => !controller.load.value
            ? Container(
                decoration: BoxDecoration(color: background),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: Get.width * 0.053, top: Get.height * 0.01477),
                        child: Text(
                          GalleryController.to.data[controller.taskIndex.value].type,
                          textAlign: TextAlign.left,
                          style: typeTaskText,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: Get.width * 0.053, top: Get.height * 0.00615),
                        child: Text(
                          GalleryController.to.data[controller.taskIndex.value].city,
                          textAlign: TextAlign.left,
                          style: cityNameText,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: Get.width * 0.053, top: Get.height * 0.00615, bottom: Get.height * 0.00615),
                        child: Text(
                          "Дата: " + GalleryController.to.data[controller.taskIndex.value].date,
                          textAlign: TextAlign.left,
                          style: cityNameText,
                        ),
                      ),
                    ),
                    Divider(
                      color: black,
                      height: 1,
                      thickness: 0,
                      indent: 0,
                      endIndent: 0,
                    ),
                    Container(
                      height: Get.height * 0.774,
                      width: Get.width,
                      margin: EdgeInsets.only(left: Get.width * 0.053, right: Get.width * 0.053),
                      child: Stack(
                        children: [
                          ListView.separated(
                            padding: EdgeInsets.only(bottom: Get.height * 0.125, top: Get.height * 0.00985),
                            itemCount: controller.data.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                width: Get.width,
                                color: controller.getColor(controller.data[index].subtitle),
                                child: InkWell(
                                  // onTap: () => controller.data[index].isBroken
                                  //     ? controller.editPorchIsBroken(controller.data[index].id)
                                  //     : controller.editPorch(controller.data[index].porchId, controller.data[index].id),
                                  onTap: () => controller.editPorchIsBroken(controller.data[index].id),
                                  child: ListTile(
                                    title: Text(controller.data[index].title,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(color: white, fontFamily: 'SanFrancisco', fontSize: 15)),
                                    subtitle: Text(controller.data[index].subtitle,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(color: white, fontFamily: 'SanFrancisco', fontSize: 13)),
                                  ),
                                ),
                              );
                            },
                            separatorBuilder: (BuildContext context, int index) => const Divider(),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              margin: EdgeInsets.only(bottom: Get.height * 0.04926),
                              width: Get.width,
                              height: Get.height * 0.07142,
                              child: TextButton(
                                onPressed: () {
                                  Get.dialog(
                                    AlertDialog(
                                      title: Text('Выгрузка'),
                                      content: Text('Выгрузить все фото (Полная) или только неотправленные (Обычная)?'),
                                      actions: [
                                        TextButton(
                                          child: Text(
                                            "Полная",
                                            style: actionButtonText,
                                          ),
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all<Color>(green),
                                          ),
                                          onPressed: () {
                                            controller.sendData(GalleryController.to.data[controller.taskIndex.value].pk, true);
                                            Get.back();
                                          },
                                        ),
                                        TextButton(
                                          child: Text(
                                            "Обычная",
                                            style: actionButtonText,
                                          ),
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all<Color>(red),
                                          ),
                                          onPressed: () {
                                            controller.sendData(GalleryController.to.data[controller.taskIndex.value].pk, false);
                                            Get.back();
                                          },
                                        ),
                                      ],
                                    ),
                                  );
                                },
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all<Color>(grey),
                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
                                child: Text(
                                  "Выгрузить",
                                  textAlign: TextAlign.center,
                                  style: buttonText,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ))
            : Center(
                child: SizedBox(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CupertinoActivityIndicator(
                        radius: 30,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: Get.height * 0.00615),
                          child: Text(
                            "${controller.countImage}/${controller.fullImage} шт.\n${controller.step.value}\nВыгружаю фотографии",
                            textAlign: TextAlign.center,
                            style: textLoader,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
