import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reklamalift/controller/address_controller.dart';
import 'package:reklamalift/controller/porch_controller.dart';
import 'package:reklamalift/controller/shield_status_controller.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:get/get.dart';
import 'package:reklamalift/custom_widgets/popup_menu_page.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';

// ignore: must_be_immutable
class PorchView extends StatelessWidget {
  final controller = Get.put(PorchController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (controller.load.value) {
          return Container(
            child: Center(
                child: CupertinoActivityIndicator(
              radius: 20,
            )),
            color: white,
          );
        } else {
          return WillPopScope(
            onWillPop: () async => false,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: orange,
                title: const Text(
                  "Подъезды",
                  textAlign: TextAlign.center,
                  style: appBarText,
                ),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () => Get.offAndToNamed('/address'),
                ),
                actions: <Widget>[
                  childPopupPage(),
                ],
                centerTitle: true,
              ),
              body: Container(
                decoration: BoxDecoration(color: background),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: Get.width * 0.0547, top: Get.height * 0.01477),
                        child: Text(
                          TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].type,
                          textAlign: TextAlign.left,
                          style: typeTaskText,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: Get.width * 0.0547, top: Get.height * 0.00615),
                        child: Text(
                          TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].city,
                          textAlign: TextAlign.left,
                          style: cityNameText,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: Get.width * 0.0547, top: Get.height * 0.00615),
                        child: Text(
                          AddressController.to.data[int.parse(controller.addressIndex.value)].address,
                          textAlign: TextAlign.left,
                          style: cityNameText,
                        ),
                      ),
                    ),
                     Divider(
                      color: black,
                      height: Get.height * 0.00615,
                      thickness: 0,
                      indent: 0,
                      endIndent: 0,
                    ),
                    Container(
                      width: Get.width,
                      height: Get.height - MediaQuery.of(context).padding.top - kToolbarHeight - Get.height * 0.235,
                      margin: EdgeInsets.only(left: Get.width * 0.0547, right: Get.width * 0.0547),
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: controller.data.length,
                        itemBuilder: (context, index) {
                          return _porchCard(index);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget _porchCard(index) {
    return Container(
        width: Get.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        child: Card(
            semanticContainer: true,
            child: InkWell(
                onTap: () {
                  Get.toNamed('/shield_status');
                  Get.lazyPut(() => ShieldStatusController());
                  ShieldStatusController.to.porchIndex.value = index.toString();
                  ShieldStatusController.to.getShieldStatusList();
                },
                child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, mainAxisSize: MainAxisSize.min, children: [
                  ListTile(
                    leading: Icon(Icons.style, color: grey),
                    title: Text(
                      "${controller.data[index].type} №${controller.data[index].porchNumber}",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: "SanFrancisco",
                      ),
                    ),
                  ),
                ]))));
  }
}
