import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:reklamalift/Models/MarkersModel.dart';
import 'package:reklamalift/controller/address_controller.dart';
import 'package:reklamalift/controller/task_controller.dart';
import 'package:reklamalift/http/url.dart';
import 'package:reklamalift/utility/colors.dart';
import 'package:reklamalift/utility/text_style.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class MapsView extends StatefulWidget {
  @override
  MapsWidgetState createState() => MapsWidgetState();
}

class MapsWidgetState extends State<MapsView> {
  Completer<GoogleMapController> _controller = Completer();
  LatLng center;
  bool _load = true;
  List<Marker> markers = <Marker>[];

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  void initState() {
    searchNearby();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: orange,
        title: const Text(
          "Карта поверхностей",
          textAlign: TextAlign.center,
          style: appBarText,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Get.offAndToNamed('/address'),
        ),
        centerTitle: true,
      ),
      body: _load
          ? Stack(children: [
              Container(
                constraints: BoxConstraints.tightFor(),
                decoration: BoxDecoration(
                  color: white,
                ),
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      right: 0,
                      bottom: 0,
                      child: GoogleMap(
                        onMapCreated: _onMapCreated,
                        markers: Set<Marker>.of(markers),
                        initialCameraPosition: CameraPosition(
                          target: center,
                          zoom: 11.0,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ])
          : Center(
              child: CupertinoActivityIndicator(
                radius: 40,
              ),
            ),
    );
  }

  // ignore: missing_return
  Future<List<MarkersModel>> searchNearby() async {
    try {
      setState(() {
        markers.clear();
      });
      setState(() {
        _load = false;
      });
      final cityCoord = await http.get(
          Uri.parse(API_URL + getCityCoord + TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].pk.toString() + '/'));
      if (cityCoord.statusCode == 200) {
        List jsonResponse = json.decode(utf8.decode(cityCoord.bodyBytes));
        double coordX = double.parse(jsonResponse[0]['coord_x']);
        double coordY = double.parse(jsonResponse[0]['coord_y']);
        center = new LatLng(coordY, coordX);
      }
      final response = await http.get(
          Uri.parse(API_URL + getTaskCoord + TaskController.to.data[int.parse(AddressController.to.taskIndex.value)].pk.toString() + '/'));
      if (response.statusCode == 200) {
        setState(() {});
        List jsonResponse = json.decode(utf8.decode(response.bodyBytes));
        List<MarkersModel> data = jsonResponse.map((job) => new MarkersModel.fromJson(job)).toList();
        setState(() {
          _load = true;
        });
        _handleResponse(data);
      } else {
        throw Exception('An error occurred getting places nearby');
      }
    } catch (exception, stackTrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
    }
  }

  void _handleResponse(data) {
    setState(() {
      List<MarkersModel> places;
      places = data;
      for (int i = 0; i < places.length; i++) {
        String porchCount = places[i].porchCount;
        markers.add(
          Marker(
            markerId: MarkerId(places[i].pk),
            position: LatLng(places[i].coord_y, places[i].coord_x),
            infoWindow: InfoWindow(title: places[i].address, snippet: 'Подъездов: $porchCount'),
            onTap: () {},
          ),
        );
      }
    });
  }
}
